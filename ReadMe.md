# Kotloid

Kotloid is a swiss tournament manager for Android Netrunner.

It allows you to enter players, and manage swiss rounds.

## Features

- Swiss round pairings
- Timer integrations to set round alarms
- Rank players automatically as points are reported
- Assign Byes+Pairings automatically
- Data import/Sync from Netrunner DB
- Tournament History

## Quick Start

1. One Time Setup: Go To Factions and either add Identities+Factions via the user interface or click Import From NRDB
2. Go to tournaments, click New Tournament
3. Add players with their name+ idenenties
4. Start Pairing Rounds
    1. Click Add
    2. Click Pair
    3. As rounds are reported, click the pairing to assigne points
    4. Repeat till done with tournament
