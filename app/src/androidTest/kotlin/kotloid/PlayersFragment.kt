package kotloid

import android.support.test.espresso.Espresso.onData
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.assertion.ViewAssertions.doesNotExist
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.matcher.BoundedMatcher
import android.support.test.espresso.matcher.ViewMatchers.*
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.Matchers.equalTo
import org.netrunner.kotloid.R
import org.netrunner.kotloid.players.PlayerVm

object PlayersFragment{
    fun openAddPlayer(): EditPlayer {
        onView(withId(R.id.addPlayerToTournament))
                .check(matches(isClickable()))
                .check(matches(isDisplayed()))
                .perform(click())
        Thread.sleep(500)
        return EditPlayer
    }

    fun hasPlayer(s: String): PlayersFragment {
        onData(withText(s))
              .check(matches(isDisplayed()))
        return PlayersFragment
    }
    fun doesNotHavePlayer(s: String): PlayersFragment{
        onData(withText(s))
                .check(doesNotExist())
        return PlayersFragment

    }

    fun withItemContent(matcher: Matcher<String>): Matcher<Any>{
        checkNotNull(matcher)
        return object: BoundedMatcher<Any, PlayerVm>(PlayerVm::class.java){
            override fun describeTo(description: Description?) {
                description?.let {
                    it.appendText("with item content")
                    matcher.describeTo(it)
                }
            }
            override fun matchesSafely(item: PlayerVm?): Boolean {
                return matcher.matches(item?.nickName)
            }
        }
    }

    fun withName(n: String): Matcher<Any> {
        checkNotNull(n)
        return withItemContent(equalTo(n))
    }
}