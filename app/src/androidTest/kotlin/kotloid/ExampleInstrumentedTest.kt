package kotloid

import android.support.test.filters.LargeTest
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.netrunner.kotloid.MainNav
import org.netrunner.kotloid.R


/**
 * Instrumentation test, which will execute on an Android device.
 * RIP Kotlintest in this case.  Gota go J-Unit style.
 * @see [Testing documentation](http://d.android.com/tools/testing)
 */
@RunWith(AndroidJUnit4::class)
@LargeTest
class KotlinInstrumentedTest {
    @Rule  @JvmField
    var mainNav = ActivityTestRule<MainNav>(MainNav::class.java)


    @Test
    @Throws(Exception::class)
    fun canAddPlayersToNewTournament() {
        MainNavigation.navigateTo(R.id.nav_new_tournament)
        PlayersFragment.openAddPlayer()
                .playerName("Ryan Bryan")
                .corp(TestData.FactionManager.aCorp().name)
                .runner(TestData.FactionManager.aRunner().name)
                .save()
                .hasPlayer("Ryan Bryan")
    }

    @Test
    @Throws(Exception::class)
    fun canDeletePlayersFromANewTournament(){
        MainNavigation.navigateTo(R.id.nav_new_tournament)
        PlayersFragment.openAddPlayer()
                .playerName("Ryan Bryan")
                .corp(TestData.FactionManager.aCorp().name)
                .runner(TestData.FactionManager.aRunner().name)
                .save()
                .hasPlayer("Ryan Bryan")
                .openAddPlayer()
                .delete()
                .doesNotHavePlayer("Ryan Bryan")

    }
}