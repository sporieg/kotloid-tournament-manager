package kotloid

import android.support.test.espresso.Espresso
import android.support.test.espresso.action.ViewActions
import android.support.test.espresso.assertion.ViewAssertions
import android.support.test.espresso.matcher.ViewMatchers
import org.hamcrest.Matchers
import org.netrunner.data.IFactionDataManager
import org.netrunner.data.InMemoryFactionDataManager
import org.netrunner.tournament.Faction
import org.netrunner.tournament.Identity

object TestData{
    var FactionManager: IFactionDataManager
    init{

        val fm = InMemoryFactionDataManager()
        fm.save(listOf(Faction("HB", "654470", false, "Hass Bioroid", "corp"),
                Faction("J", "d64026", false, "Jinteki", "corp"),
                Faction("W", "2b7049", false, "Weyland", "corp"),
                Faction("N", "353836", false, "Neutral", "corp"),
                Faction("A", "f22a02", false, "Anarch", "runner"),
                Faction("C", "1914ba", false, "Criminal", "runner"),
                Faction("S", "0dd813", false, "Shaper", "runner"),
                Faction("RN", "546855", false, "Neutral", "runner"),
                Faction("Adam", "c9d8ca", true, "Adam", "runner")))
        fm.saveIdentities(listOf(Identity("00005", "Noise", "A"),
                Identity("00006", "Kate", "S"),
                Identity("00007", "Steve", "C"),
                Identity("00008", "Adam", "Adam"),
                Identity("00008", "Hackerman", "RN"),
                Identity("00009", "Weyland: BABW", "W"),
                Identity("00010", "Hass Bioroid: The only one", "HB"),
                Identity("00011", "Jinteki: Evolution", "J"),
                Identity("00012", "Shadow: Doing Something", "N")))
        FactionManager =  fm
    }
}

fun setSpinner(id: Int, value: String){
    Espresso.onView(ViewMatchers.withId(id)).perform(ViewActions.click())
    Espresso.onView(ViewMatchers.withText(value)).perform(ViewActions.click())
    Espresso.onView(ViewMatchers.withId(id)).check(ViewAssertions.matches(ViewMatchers.withSpinnerText(Matchers.containsString(value))))
}

fun setTextView(id: Int, value: String){
    Espresso.onView(ViewMatchers.withId(id))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
            .perform(ViewActions.typeText(value), ViewActions.closeSoftKeyboard())
}