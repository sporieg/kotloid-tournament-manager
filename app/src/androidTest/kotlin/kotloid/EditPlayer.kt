package kotloid

import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.matcher.ViewMatchers.withId
import org.netrunner.kotloid.R

object EditPlayer {
    fun playerName(value: String): EditPlayer{
        setTextView(R.id.playerNickName, value)
        return EditPlayer
    }
    fun corp(value: String): EditPlayer {
        setSpinner(R.id.corp_spinner, value)
        return EditPlayer
    }
    fun runner(value: String): EditPlayer {
        setSpinner(R.id.runner_spinner, value)
        return EditPlayer
    }
    fun delete(): PlayersFragment{
        onView(withId(R.id.delete_player)).perform(click())
        Thread.sleep(500)
        return PlayersFragment
    }
    fun save(): PlayersFragment {
        onView(withId(R.id.save_player)).perform(click())
        Thread.sleep(500)
        return PlayersFragment
    }
}