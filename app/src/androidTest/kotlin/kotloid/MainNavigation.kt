package kotloid

import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.contrib.DrawerActions.open
import android.support.test.espresso.contrib.DrawerMatchers.isOpen
import android.support.test.espresso.contrib.NavigationViewActions
import android.support.test.espresso.matcher.ViewMatchers.withId
import org.netrunner.kotloid.R

object MainNavigation {
    fun navigateTo(option: Int){
        onView(withId(R.id.drawer_layout)).perform(open())
        onView(withId(R.id.drawer_layout)).check(matches(isOpen()))
        onView(withId(R.id.nav_view)).perform(NavigationViewActions.navigateTo(option))
        Thread.sleep(500)
    }
}