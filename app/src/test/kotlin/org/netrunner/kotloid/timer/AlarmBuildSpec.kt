package org.netrunner.kotloid.timer

import io.kotlintest.KTestJUnitRunner
import io.kotlintest.matchers.shouldBe
import io.kotlintest.specs.FeatureSpec
import org.junit.runner.RunWith
import java.util.*

@RunWith(KTestJUnitRunner::class)
class AlarmBuildSpec: FeatureSpec(){
    init{
        feature("Can build alarms for a minute amount"){
            val c = Calendar.getInstance()
            c.set(2018, 8, 5, 11, 0, 0) //Wed Sep 05 11:00:00 CDT 2018
            val testNow = c.time
            scenario("Get minute properly for android alarm"){
                val alarmTime = AlarmTimeBuilder(15, Calendar.getInstance(), testNow)
                alarmTime.minutes shouldBe 15
            }
            scenario("Overflow time handled fine"){
                val alarmTime = AlarmTimeBuilder(65, Calendar.getInstance(), testNow)
                alarmTime.minutes shouldBe 5
            }
            scenario("Get hour properly for android alarm"){
                val alarmTime = AlarmTimeBuilder(15, Calendar.getInstance(), testNow)
                alarmTime.hour shouldBe 11
            }
            scenario("Overflow hour handled fine"){
                val alarmTime = AlarmTimeBuilder(65, Calendar.getInstance(), testNow)
                alarmTime.hour shouldBe 12
            }
            scenario("Overflow days handled fine"){
                val testHours = 13 * 60 + 15 //13 hours 15 minutes
                val alarmTime = AlarmTimeBuilder(testHours, Calendar.getInstance(), testNow)
                alarmTime.minutes shouldBe 15
                alarmTime.hour shouldBe 0 //Tomorrow
            }
            scenario("Can get millis rounded"){
                val expected = Calendar.getInstance()
                expected.set(2018, 8, 5, 11, 15, 0)
                expected.set(Calendar.MILLISECOND, 0)

                val alarmTime = AlarmTimeBuilder(15, Calendar.getInstance(), testNow)

                alarmTime.millis shouldBe expected.timeInMillis

            }

        }
    }
}