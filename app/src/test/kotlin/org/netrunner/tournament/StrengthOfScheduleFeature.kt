package org.netrunner.tournament

import io.kotlintest.KTestJUnitRunner
import io.kotlintest.matchers.*
import io.kotlintest.specs.FeatureSpec
import org.junit.runner.RunWith
import org.netrunner.tournament.swiss.*

@RunWith(KTestJUnitRunner::class)
class StrengthOfScheduleFeature: FeatureSpec(){
    init {
        feature("Can calc SOS before the first swissRound"){
            val t: SwissTournament = SwissTournament()
            val testCorp = TestData.testCorp
            val testRunner = TestData.testRunner
            val p1 = t.addPlayer("p1", testCorp, testRunner)
            val p2 = t.addPlayer("p2", testCorp, testRunner)
            val p3 = t.addPlayer("p3", testCorp, testRunner)
            val p4 = t.addPlayer("p4", testCorp, testRunner)
            val p5 = t.addPlayer("p5", testCorp, testRunner)
            val round1 = SwissRound(0, listOf(SwissBye(p1.id),
                    SwissPlayerPairing(p2.id, p3.id),
                    SwissPlayerPairing(p4.id, p5.id)))
            t.addRound(round1)
            scenario("SwissBye editPlayer has no Sos"){
                t.matchHistoryFor(p1) should beEmpty()
                p1.strengthOfSchedule(t) shouldBe 0e10
            }
            scenario("paired players have no SoS"){
                p2.strengthOfSchedule(t) shouldBe 0e10
                p3.strengthOfSchedule(t) shouldBe 0e10
                p4.strengthOfSchedule(t) shouldBe 0e10
                p5.strengthOfSchedule(t) shouldBe 0e10
            }
        }
        feature("Calc Sos after 3 rounds"){
            val t = TestData.ThreeRoundTournament.t
            val p1 = TestData.ThreeRoundTournament.p1
            val p2 = TestData.ThreeRoundTournament.p2
            val p3 = TestData.ThreeRoundTournament.p3
            val p4 = TestData.ThreeRoundTournament.p4
            val p5 = TestData.ThreeRoundTournament.p5
            scenario("Player 1'corpIdKey SoS from opponents (p5, p3)"){
                p1.strengthOfSchedule(t) shouldBe exactly(4.0)
            }
            scenario("Player 2'corpIdKey SoS from opponents (p3, p4)"){
                p2.strengthOfSchedule(t) shouldBe exactly(4.0)
            }
            scenario("Player 3'corpIdKey SoS from opponents (p2,p1,p4)"){
                p3.strengthOfSchedule(t) shouldBe (3.3333 plusOrMinus .0001)
            }
            scenario("Player 4'corpIdKey SoS from opponents (p5,p2,p3))"){
                p4.strengthOfSchedule(t) shouldBe (3.6666 plusOrMinus .0001)
            }
            scenario("Player 5'corpIdKey SoS from opponents (p4,p1)"){
                p5.strengthOfSchedule(t) shouldBe exactly(3.5)
            }
        }
    }
}