package org.netrunner.tournament

import io.kotlintest.matchers.shouldEqual
import io.kotlintest.specs.StringSpec

class FactionSpec: StringSpec(){
    init {
        "Factions are the same with the same id"{
            Faction() shouldEqual Faction()
            Faction(id ="5") shouldEqual Faction(id = "5")
            Faction(id ="5", color = "000000") shouldEqual Faction(id = "5", color = "FFFFFF")
        }
    }
}