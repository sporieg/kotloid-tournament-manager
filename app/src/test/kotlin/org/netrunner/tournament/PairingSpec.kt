package org.netrunner.tournament

import io.kotlintest.KTestJUnitRunner
import io.kotlintest.forOne
import io.kotlintest.matchers.*
import io.kotlintest.specs.FeatureSpec
import org.junit.runner.RunWith
import org.netrunner.tournament.TestData.CorpSplit
import org.netrunner.tournament.TestData.Player1Sweep
import org.netrunner.tournament.TestData.RunnerSplit
import org.netrunner.tournament.swiss.*

@RunWith(KTestJUnitRunner::class)
class PairingSpec: FeatureSpec(){
    init{
        feature("Can rank players"){
            scenario("Rank by Points, then Sos, then eSos, then random"){
                val t = TestData.ThreeRoundTournament
                val ranker = SwissRanker(t.t)
                ranker.rankedPlayers()shouldBe listOf(t.p4, t.p5, t.p3, t.p2, t.p1)
            }
            scenario("Rank players would have not played yet"){
                val t = TestData.NoRoundsPlayed
                val ranker = SwissRanker(t.t)
                ranker.rankedPlayers()should containsAll(t.p4, t.p5, t.p3, t.p2, t.p1)
            }
            scenario("Can get a list of the current participants (no drops)"){
                val t = TestData.OneRoundTournamentWithADrop
                t.t.activePlayers() should containsAll(t.p1, t.p2, t.p3, t.p4, t.p6)
            }
        }
        feature("Can award byes"){
            scenario("Can tell who already got a bye"){
                val t = TestData.ThreeRoundTournament
                t.t.byePlayers() should containsAll(t.p1, t.p2, t.p5)
                t.t.noByePlayers() should containsAll(t.p3, t.p4)
            }
            scenario("Award a by randomly first swissRound"){
                val d = TestData.NoRoundsPlayed
                val t = d.t
                val matcher = GenSwissPairing(t, 1)
                val p = matcher.awardBye()
                listOf(d.p1, d.p2, d.p3, d.p3, d.p4, d.p5) should contain(p)
            }
            scenario("Award two lowest points"){}
            scenario("Assign bye to lowest points who did not already have a bye"){}
        }
        feature("Can generate pairings"){
            scenario("Throw exception for out of bounds swissRound"){
                val t = TestData.NoRoundsPlayed()
                val matcher = SwissRoundGenerator(t)
                shouldThrow<Error>{
                    matcher.pairRound(3)
                }
            }
            scenario("Match by random first swissRound"){
                val t = TestData.NoRoundsPlayed()
                t.addRound()
                val matcher = SwissRoundGenerator(t)
                val r = matcher.pairRound(1)
                r.pairings.count() shouldBe 3
            }
            scenario("No byes when all can be paired."){
                val t = TestData.NoRoundsPlayed()
                t.players.last().dropped = true
                t.addRound()
                val matcher = SwissRoundGenerator(t)
                val r = matcher.pairRound(1)
                r.pairings.count() shouldBe 2
            }
            scenario("Match by points first"){
                val t = TestData.TwoRoundsPlayed()
                val p1 = t.players.first { it.nickName == "p1" }
                val p2 = t.players.first { it.nickName == "p2" }
                val p3 = t.players.first { it.nickName == "p3" }
                val p4 = t.players.first { it.nickName == "p4" }
                val p5 = t.players.first { it.nickName == "p5" }
                val matcher = SwissRoundGenerator(t)
                matcher.pairRound(3).shouldEqual(SwissRound(3, listOf(SwissBye(p2, 3), CorpSplit(p5, p3), RunnerSplit(p4, p1))))
            }
            scenario("Match a larger tournament points first"){
                val t = SwissTournament()
                val zero = t.addPlayer("Zero", Identity(), Identity())
                val one = t.addPlayer("One", Identity(), Identity())
                val two = t.addPlayer("Two", Identity(), Identity())
                val three = t.addPlayer("Three", Identity(), Identity())
                val four = t.addPlayer("Four", Identity(), Identity())
                val five = t.addPlayer("Five", Identity(), Identity())
                val b2 = t.addPlayer("2B", Identity(), Identity())
                val s9 = t.addPlayer("9S", Identity(), Identity())
                val A2 = t.addPlayer("A2", Identity(), Identity())
                val sixPointers = listOf(zero, two, four, b2, A2).map { it.id }
                val zeroHeroes = listOf(one, three, five, s9).map { it.id }
                t.addRound(SwissRound(1, listOf(
                        Player1Sweep(zero, one),
                        Player1Sweep(two, three),
                        Player1Sweep(four, five),
                        Player1Sweep(b2, s9),
                        SwissBye(A2))))
                t.addRound()
                val matcher = SwissRoundGenerator(t)
                val lastRound = matcher.pairRound(2)
                val pairs: List<SwissPlayerPairing> = lastRound.pairings.filter { it is SwissPlayerPairing }.map { it as SwissPlayerPairing }
                val bye = lastRound.pairings.first { it is SwissBye } as SwissBye

                forOne(pairs){
                    sixPointers should contain(it.p1)
                    zeroHeroes should contain(it.p2)
                }
                zeroHeroes should contain(bye.byePlayer)
            }
            scenario("Match a large tournament, 3rd swissRound"){
                val t = SwissTournament()
                val zero =t.addPlayer("Zero", Identity(), Identity())
                val one = t.addPlayer("One", Identity(), Identity())
                val two = t.addPlayer("Two", Identity(), Identity())
                val three = t.addPlayer("Three", Identity(), Identity())
                val four = t.addPlayer("Four", Identity(), Identity())
                val five = t.addPlayer("Five", Identity(), Identity())
                val b2 = t.addPlayer("2B", Identity(), Identity())
                val s9 = t.addPlayer("9S", Identity(), Identity())
                val A2 = t.addPlayer("A2", Identity(), Identity())
                t.addRound(SwissRound(1, listOf(
                        SwissBye(s9),
                        CorpSplit(zero, one),
                        CorpSplit(two, three),
                        RunnerSplit(four, five),
                        Player1Sweep(A2, b2)
                )))
                t.addRound(SwissRound(2, listOf(
                        SwissBye(b2),
                        CorpSplit(zero, three),
                        CorpSplit(two, five),
                        RunnerSplit(four, one),
                        Player1Sweep(A2, s9)
                )))
                t.addRound()
                val gen = SwissRoundGenerator(t)
                val r = gen.pairRound(3)
                t.assignPairings(r)
                val pairs = r.pairings.count()
                pairs shouldBe 5
                //Run this test 500 times as there are many chances to fudge up player swissRound assignment.
            }.config(invocations = 500)
        }
    }
}