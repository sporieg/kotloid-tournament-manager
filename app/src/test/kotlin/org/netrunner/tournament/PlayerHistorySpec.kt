package org.netrunner.tournament

import io.kotlintest.KTestJUnitRunner
import io.kotlintest.matchers.*
import io.kotlintest.specs.FeatureSpec
import org.junit.runner.RunWith
import org.netrunner.tournament.swiss.*

@RunWith(KTestJUnitRunner::class)
class PlayerHistorySpec: FeatureSpec(){
    init{
        val t: SwissTournament = TestData.ThreeRoundTournament.t
        val p1 = TestData.ThreeRoundTournament.p1
        val p2 = TestData.ThreeRoundTournament.p2
        val p3 = TestData.ThreeRoundTournament.p3
        val p4 = TestData.ThreeRoundTournament.p4
        val p5 = TestData.ThreeRoundTournament.p5
        feature("Track editPlayer points history"){
            scenario("Everyone's points are correct after 3 rounds"){
                p1.points(t) shouldBe 6 + 0 + 3
                p2.points(t) shouldBe 3 + 0 + 6
                p3.points(t) shouldBe 3 + 6 + 3
                p4.points(t) shouldBe 3 + 6 + 3
                p5.points(t) shouldBe 3 + 6 + 3
            }
            scenario("Match and swissRound histories count correctly"){
                t.matchHistoryFor(p1).size shouldBe 2
                t.roundHistoryFor(p1).size shouldBe 3
            }
            scenario("Players swissRound history counts correct"){
                p1.rounds(t) shouldBe 3
                p2.rounds(t) shouldBe 3
                p3.rounds(t) shouldBe 3
                p4.rounds(t) shouldBe 3
                p5.rounds(t) shouldBe 3
            }
            scenario("Players average points per swissRound count correct"){
                p1.avgPoints(t) shouldBe exactly(3.0)
                p2.avgPoints(t) shouldBe exactly(3.0)
                p3.avgPoints(t) shouldBe exactly(4.0)
                p4.avgPoints(t) shouldBe exactly(4.0)
                p5.avgPoints(t) shouldBe exactly(4.0)
            }
        }
        feature("Can get each players opponent list"){
            scenario("get each players opponents for 3 swissRound tourny"){
                p1.opponents(t) should containsAll(p3, p5)
                p1.opponents(t) should haveSize(2)
                p2.opponents(t) should containsAll(p3, p4)
                p1.opponents(t) should haveSize(2)
                p3.opponents(t) should containsAll(p2, p1, p4)
                p1.opponents(t) should haveSize(2)
                p4.opponents(t) should containsAll(p5, p2,p3)
                p1.opponents(t) should haveSize(2)
                p5.opponents(t) should containsAll(p4, p1)
                p1.opponents(t) should haveSize(2)
            }
        }
    }
}