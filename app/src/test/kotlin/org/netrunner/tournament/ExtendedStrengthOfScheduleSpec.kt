package org.netrunner.tournament

import io.kotlintest.KTestJUnitRunner
import io.kotlintest.matchers.plusOrMinus
import io.kotlintest.matchers.shouldBe
import io.kotlintest.specs.FeatureSpec
import org.junit.runner.RunWith
import org.netrunner.tournament.swiss.extendedStrengthOfSchedule

@RunWith(KTestJUnitRunner::class)
class ExtendedStrengthOfScheduleSpec: FeatureSpec(){
    init{
        val p1 = TestData.ThreeRoundTournament.p1
        val p2 = TestData.ThreeRoundTournament.p2
        val p3 = TestData.ThreeRoundTournament.p3
        val p4 = TestData.ThreeRoundTournament.p4
        val p5 = TestData.ThreeRoundTournament.p5
        val t = TestData.ThreeRoundTournament.t
        feature("Track the editPlayer'corpIdKey extended sos"){
            scenario("Player 1'corpIdKey eSoS from opponents (p5, p3)"){
                p1.extendedStrengthOfSchedule(t) shouldBe (3.4167 plusOrMinus .0001)
            }
            scenario("Player 2'corpIdKey eSoS from opponents (p3, p4)"){
                p2.extendedStrengthOfSchedule(t) shouldBe (3.5 plusOrMinus .0001)
            }
            scenario("Player 3'corpIdKey eSoS from opponents (p2,p1,p4)"){
                p3.extendedStrengthOfSchedule(t) shouldBe (3.8889 plusOrMinus .0001)
            }
            scenario("Player 4'corpIdKey eSoS from opponents (p5,p2,p3))"){
                p4.extendedStrengthOfSchedule(t) shouldBe (3.6111 plusOrMinus .0001)
            }
            scenario("Player 5'corpIdKey eSoS from opponents (p4,p1)"){
                p5.extendedStrengthOfSchedule(t) shouldBe (3.8333 plusOrMinus .0001)
            }
        }
    }
}
