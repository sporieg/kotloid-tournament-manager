package org.netrunner.tournament

import io.kotlintest.KTestJUnitRunner
import io.kotlintest.specs.FeatureSpec
import org.junit.runner.RunWith
import io.kotlintest.matchers.*
import org.netrunner.tournament.swiss.SwissTournament

@RunWith(KTestJUnitRunner::class)
class TournamentSpec: FeatureSpec(){
    init {
        feature("Can add players to the tourney"){
            scenario("Add to an empty tourney"){
                val t = SwissTournament()
                t.addPlayer("foo", TestData.testCorp, TestData.testRunner)
                t.players should haveSize(1)
            }
        }
    }
}