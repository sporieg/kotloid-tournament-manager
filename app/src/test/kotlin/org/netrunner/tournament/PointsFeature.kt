package org.netrunner.tournament

import io.kotlintest.KTestJUnitRunner
import io.kotlintest.matchers.should
import io.kotlintest.matchers.shouldBe
import io.kotlintest.matchers.shouldThrow
import io.kotlintest.matchers.startWith
import io.kotlintest.specs.FeatureSpec
import org.junit.runner.RunWith
import org.netrunner.tournament.Points.ModifiedWin
import org.netrunner.tournament.swiss.SwissPlayerPairing
import org.netrunner.tournament.swiss.SwissRound
import org.netrunner.tournament.swiss.SwissTournament
import org.netrunner.tournament.swiss.points

@RunWith(KTestJUnitRunner::class)
class PointsFeature : FeatureSpec() {
    init {
        feature("can calculate editPlayer points") {
            val testCorp = Identity(name = "Comcast", factionId = Identity.CORP)
            val testRunner = Identity(name = "Steve", factionId = Identity.RUNNER)
            val t = SwissTournament(mutableListOf(), mutableListOf())
            val player1 = t.addPlayer("Johny", testCorp, testRunner)
            val player2 = t.addPlayer("Steve'corpIdKey Clone", testCorp, testRunner)
            val player3 = t.addPlayer("Steve'corpIdKey Other Clone", testCorp, testRunner)
            val round1Pairing = SwissPlayerPairing(player1.id, player2.id)
            val round1 = SwissRound(1, listOf(round1Pairing))
            t.addRound(round1)

            round1Pairing.match.game1.corpPoints = ModifiedWin.points
            round1Pairing.match.game2.runnerPoints = ModifiedWin.points

            scenario("get points for runner and corp wins") {
                player1.points(t) shouldBe 4
            }
            scenario("No wins, no points") {
                player2.points(t) shouldBe 0
            }
            scenario("Asking a game for the points of an uninvolved editPlayer returns 0"){
                round1Pairing.match.game1.pointsFor(player3.id) shouldBe 0
            }
            scenario("A pairing can tell you the opponentOf person"){
                round1Pairing.opponentOf(player1.id) shouldBe player2.id
            }
            scenario("Asking for the opponentOf person of a swissRound you were not in does not make sense and is an exception"){
                val exception = shouldThrow<Exception> {
                    round1Pairing.opponentOf(player3.id)
                }
                exception.message.should { startWith("Player did not play")}
            }
            scenario("Can assign points via editPlayer"){
                val t1 = SwissTournament(mutableListOf(), mutableListOf())
                val p1 = t.addPlayer("Johny", testCorp, testRunner)
                val p2 = t.addPlayer("Steve'corpIdKey Clone", testCorp, testRunner)
                val p = SwissPlayerPairing(p1.id, p2.id)
                val r1 = SwissRound(1, listOf(p))
                t1.addRound(r1)

                p.match.game1.setPointsFor(p1.id, 2)
                p.match.game1.setPointsFor(p2.id, 0)
                p.match.game2.setPointsFor(p1.id, 1)
                p.match.game2.setPointsFor(p2.id, 1)

                p.match.game1.pointsFor(p1.id) shouldBe 2
                p.match.game1.pointsFor(p2.id) shouldBe 0
                p.match.game2.pointsFor(p1.id) shouldBe 1
                p.match.game2.pointsFor(p1.id) shouldBe 1
                p.pointsFor(p1.id) shouldBe 3
                p.pointsFor(p2.id) shouldBe 1
            }
        }
        feature("Can map ints back to Points data"){
            scenario("0 points is a loss"){
                Points.Loss.matches(0) shouldBe true
            }
            scenario("1 points is a draw"){
                Points.Draw.matches(1) shouldBe true
            }
            scenario("2 points is a modified win"){
                Points.ModifiedWin.matches(2) shouldBe true
            }
            scenario("3 points is a Win"){
                Points.Win.matches(3) shouldBe true
            }
        }
    }
}