package org.netrunner.tournament

import io.kotlintest.KTestJUnitRunner
import io.kotlintest.matchers.shouldBe
import io.kotlintest.matchers.shouldEqual
import io.kotlintest.specs.StringSpec
import org.junit.runner.RunWith
import org.netrunner.tournament.swiss.SwissGame
import org.netrunner.tournament.swiss.SwissMatch
import org.netrunner.tournament.swiss.SwissPlayerPairing
import org.netrunner.tournament.swiss.SwissTournament

@RunWith(KTestJUnitRunner::class)
class PairingTest: StringSpec(){
    init {
        "Pairings with the same players are equal" {
            val t = SwissTournament()
            val p1 = t.addPlayer("p1", TestData.testCorp, TestData.testRunner)
            val p2 = t.addPlayer("p2", TestData.testCorp, TestData.testRunner)
            val pair1 = SwissPlayerPairing(p1.id, p2.id)
            val pair2 = SwissPlayerPairing(p1.id, p2.id)
            pair1 shouldEqual pair2
        }
        "Pairings with the same players out of order are equal" {
            val t = SwissTournament()
            val p1 = t.addPlayer("p1", TestData.testCorp, TestData.testRunner)
            val p2 = t.addPlayer("p2", TestData.testCorp, TestData.testRunner)
            val pair1 = SwissPlayerPairing(p1.id, p2.id)
            val pair2 = SwissPlayerPairing(p2.id, p1.id)
            pair1 shouldEqual pair2
        }
        "First editPlayer passed to constructor is runner in the first game"{
            val t = SwissTournament()
            val p1 = t.addPlayer("p1", TestData.testCorp, TestData.testRunner)
            val p2 = t.addPlayer("p2", TestData.testCorp, TestData.testRunner)
            val g1 = SwissGame(p1.id, p2.id)
            val g2 = SwissGame(p2.id, p1.id)
            val pair1 = SwissPlayerPairing(p1.id, p2.id, match = SwissMatch(g1, g2))
            pair1.match.corpGameFor(p1.id) shouldBe g1
            pair1.match.runnerGameFor(p1.id) shouldBe g2
            pair1.match.corpGameFor(p2.id) shouldBe g2
            pair1.match.runnerGameFor(p2.id) shouldBe g1
        }
    }
}