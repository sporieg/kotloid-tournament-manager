package org.netrunner.tournament

import io.kotlintest.KTestJUnitRunner
import io.kotlintest.matchers.shouldBe
import io.kotlintest.specs.FeatureSpec
import org.junit.runner.RunWith

@RunWith(KTestJUnitRunner::class)
class PlayerSpec: FeatureSpec() {
    init{
        val d = TestData.ThreeRoundTournament
        feature("Auto generate editPlayer ids"){
            scenario("Id inc per player"){
                d.p1.id shouldBe  1
                d.p2.id shouldBe  2
                d.p3.id shouldBe  3
                d.p4.id shouldBe  4
                d.p5.id shouldBe  5
            }
        }
    }
}