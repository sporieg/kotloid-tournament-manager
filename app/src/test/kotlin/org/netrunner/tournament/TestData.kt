package org.netrunner.tournament

import org.netrunner.tournament.swiss.*


object TestData{
    val testCorp = Identity(name = "Comcast", factionId = Identity.CORP)
    val testRunner = Identity(name = "Steve", factionId = Identity.RUNNER)
    fun CorpSplit(p1: Player, p2: Player): SwissPlayerPairing {
        val game1 = SwissGame(p1.id, p2.id, Points.Win.points, Points.Loss.points)
        val game2 = SwissGame(p2.id, p1.id, Points.Win.points, Points.Loss.points)
        val match = SwissMatch(game1, game2)
        return SwissPlayerPairing(p1.id, p2.id, match = match)
    }

    fun RunnerSplit(p1: Player, p2: Player): SwissPlayerPairing {
        val game1 = SwissGame(p1.id, p2.id, Points.Loss.points, Points.Win.points)
        val game2 = SwissGame(p2.id, p1.id, Points.Loss.points, Points.Win.points)
        val match = SwissMatch(game1, game2)
        return SwissPlayerPairing(p1.id, p2.id, match = match)
    }

    fun Player1Sweep(p1: Player, p2: Player): SwissPlayerPairing {
        val game1 = SwissGame(p1.id, p2.id, Points.Win.points, Points.Loss.points)
        val game2 = SwissGame(p2.id, p1.id, Points.Loss.points, Points.Win.points)
        val match = SwissMatch(game1, game2)
        return SwissPlayerPairing(p1.id, p2.id, match = match)
    }

    fun Player2Sweep(p1: Player, p2: Player): SwissPlayerPairing {
        val game1 = SwissGame(p1.id, p2.id, Points.Loss.points, Points.Win.points)
        val game2 = SwissGame(p2.id, p1.id, Points.Win.points, Points.Loss.points)
        val match = SwissMatch(game1, game2)
        return SwissPlayerPairing(p1.id, p2.id, match = match)
    }

    fun SwissRound.sync(): SwissRound {
        pairings.forEach {
            it.round = num
        }
        return this
    }

    object NoRoundsPlayed{
        val t: SwissTournament = SwissTournament()
        val p1 = t.addPlayer("p1", testCorp, testRunner)
        val p2 = t.addPlayer("p2", testCorp, testRunner)
        val p3 = t.addPlayer("p3", testCorp, testRunner)
        val p4 = t.addPlayer("p4", testCorp, testRunner)
        val p5 = t.addPlayer("p5", testCorp, testRunner)
    }
    
    fun NoRoundsPlayed(): SwissTournament {
        val t: SwissTournament = SwissTournament()
        t.addPlayer("p1", testCorp, testRunner)
        t.addPlayer("p2", testCorp, testRunner)
        t.addPlayer("p3", testCorp, testRunner)
        t.addPlayer("p4", testCorp, testRunner)
        t.addPlayer("p5", testCorp, testRunner)
        return t
    }

    fun TwoRoundsPlayed(): SwissTournament {
        val t: SwissTournament = SwissTournament()
        val p1 = t.addPlayer("p1", testCorp, testRunner)
        val p2 = t.addPlayer("p2", testCorp, testRunner)
        val p3 = t.addPlayer("p3", testCorp, testRunner)
        val p4 = t.addPlayer("p4", testCorp, testRunner)
        val p5 = t.addPlayer("p5", testCorp, testRunner)
        val round1 = SwissRound(1, listOf(SwissBye(p1), CorpSplit(p2, p3), RunnerSplit(p4, p5))).sync()
        t.addRound(round1)
        val round2 = SwissRound(2, listOf(SwissBye(p5), Player2Sweep(p1, p3), Player1Sweep(p4, p2))).sync()
        t.addRound(round2)
        t.addRound()
        return t
    }

    object ThreeRoundTournament {
        val t: SwissTournament = SwissTournament()
        val p1 = t.addPlayer("p1", testCorp, testRunner)
        val p2 = t.addPlayer("p2", testCorp, testRunner)
        val p3 = t.addPlayer("p3", testCorp, testRunner)
        val p4 = t.addPlayer("p4", testCorp, testRunner)
        val p5 = t.addPlayer("p5", testCorp, testRunner)
        init{
            val round1 = SwissRound(1, listOf(SwissBye(p1), CorpSplit(p2, p3), RunnerSplit(p4, p5))).sync()
            t.addRound(round1)
            val round2 = SwissRound(2, listOf(SwissBye(p5), Player2Sweep(p1, p3), Player1Sweep(p4, p2))).sync()
            t.addRound(round2)
            val round3 = SwissRound(3, listOf(SwissBye(p2), CorpSplit(p1, p5), RunnerSplit(p4, p3))).sync()
            t.addRound(round3)
        }
    }

    object OneRoundTournamentWithADrop {
        val t: SwissTournament = SwissTournament()
        val p1 = t.addPlayer("p1", testCorp, testRunner)
        val p2 = t.addPlayer("p2", testCorp, testRunner)
        val p3 = t.addPlayer("p3", testCorp, testRunner)
        val p4 = t.addPlayer("p4", testCorp, testRunner)
        val p5 = t.addPlayer("p5", testCorp, testRunner)
        val p6 = t.addPlayer("p6", testCorp, testRunner)
        init{
            val round1 = SwissRound(1, listOf(RunnerSplit(p1, p2), RunnerSplit(p3, p4), RunnerSplit(p5, p6))).sync()
            t.addRound(round1)
            p5.dropped = true //Drop after one swissRound.
        }
    }
}