package org.netrunner.data

import io.kotlintest.matchers.shouldEqual
import io.kotlintest.specs.FeatureSpec
import org.netrunner.rx.AddPlayer
import org.netrunner.rx.RxBus
import org.netrunner.tournament.Faction
import org.netrunner.tournament.Identity

class TournamentEventsSpec: FeatureSpec(){
    init {
        val factionManager = InMemoryFactionDataManager()
        factionManager.save(listOf(Faction("HB", "654470", false, "Hass Bioroid", "corp"),
                Faction("J", "d64026", false, "Jinteki", "corp"),
                Faction("W", "2b7049", false, "Weyland", "corp"),
                Faction("N", "353836", false, "Neutral", "corp"),
                Faction("A", "f22a02", false, "Anarch", "runner"),
                Faction("C", "1914ba", false, "Criminal", "runner"),
                Faction("S", "0dd813", false, "Shaper", "runner"),
                Faction("RN", "546855", false, "Neutral", "runner"),
                Faction("Adam", "c9d8ca", true, "Adam", "runner")))
        factionManager.saveIdentities(listOf(Identity("00005", "Noise", "A"),
                Identity("00006", "Kate", "S"),
                Identity("00007", "Steve", "C"),
                Identity("00008", "Adam", "Adam"),
                Identity("00008", "Hackerman", "RN"),
                Identity("00009", "Weyland: BABW", "W"),
                Identity("00010", "Hass Bioroid: The only one", "HB"),
                Identity("00011", "Jinteki: Evolution", "J"),
                Identity("00012", "Shadow: Doing Something", "N")))
        val runner = factionManager.identityFactionsRunners().first().identity
        val corp = factionManager.identityFactionsRunners().first().identity
        feature("Can make tournaments"){
            scenario("Add a editPlayer to a new tournament"){
                val bus = RxBus()
                val dataManager = InMemoryDataManager()
                val tId = dataManager.newTournament().id
                TournamentEventRouter(dataManager, factionManager, bus)
                bus.send(AddPlayer("Tommy", runner, corp, tId))
                val players = dataManager.currentTournament().tournament.players.count()
                players shouldEqual 1
            }
        }
    }
}