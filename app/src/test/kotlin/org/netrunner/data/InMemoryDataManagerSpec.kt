package org.netrunner.data

import io.kotlintest.matchers.shouldBe
import io.kotlintest.matchers.shouldEqual
import io.kotlintest.matchers.shouldNotBe
import io.kotlintest.specs.FeatureSpec
import org.netrunner.tournament.Faction
import org.netrunner.tournament.Identity

class InMemoryDataManagerSpec: FeatureSpec(){
    init {
        val factionManager = InMemoryFactionDataManager()
        factionManager.save(listOf(Faction("HB", "654470", false, "Hass Bioroid", "corp"),
                Faction("J", "d64026", false, "Jinteki", "corp"),
                Faction("W", "2b7049", false, "Weyland", "corp"),
                Faction("N", "353836", false, "Neutral", "corp"),
                Faction("A", "f22a02", false, "Anarch", "runner"),
                Faction("C", "1914ba", false, "Criminal", "runner"),
                Faction("S", "0dd813", false, "Shaper", "runner"),
                Faction("RN", "546855", false, "Neutral", "runner"),
                Faction("Adam", "c9d8ca", true, "Adam", "runner")))
        factionManager.saveIdentities(listOf(Identity("00005", "Noise", "A"),
                Identity("00006", "Kate", "S"),
                Identity("00007", "Steve", "C"),
                Identity("00008", "Adam", "Adam"),
                Identity("00008", "Hackerman", "RN"),
                Identity("00009", "Weyland: BABW", "W"),
                Identity("00010", "Hass Bioroid: The only one", "HB"),
                Identity("00011", "Jinteki: Evolution", "J"),
                Identity("00012", "Shadow: Doing Something", "N")))
        feature("Can create tournaments"){
            scenario("Can get the current tournament"){
                val dataManager = InMemoryDataManager()
                val t = dataManager.currentTournament()
                t shouldNotBe null
            }
            scenario("Can get the current tournament after making a new one"){
                val dataManager = InMemoryDataManager()
                dataManager.newTournament()
                val second = dataManager.newTournament()
                val cur = dataManager.currentTournament()
                cur.id shouldEqual second.id
            }
        }
        feature("Can save players"){
            val runner = factionManager.identityFactionsRunners().first().identity
            val corp = factionManager.identityFactionsRunners().first().identity
            scenario("Add editPlayer to empty tournament"){
                val dataManager = InMemoryDataManager()
                val t = dataManager.currentTournament()
                t.tournament.addPlayer("Tom", corp, runner)
                dataManager.save(t){}
                dataManager.currentTournament().tournament.players.count() shouldBe 1
            }
        }
    }
}