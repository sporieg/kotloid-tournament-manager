package org.netrunner.rx

import io.kotlintest.KTestJUnitRunner
import io.kotlintest.matchers.shouldBe
import io.kotlintest.specs.FeatureSpec
import org.junit.runner.RunWith


@RunWith(KTestJUnitRunner::class)
class RxBusSpec: FeatureSpec(){
    init {
        feature("Can publish to subscribers"){
            val bus = RxBus()
            scenario("String message"){
                var receivedString = ""
                bus.toObservable().subscribe {
                    receivedString = it.toString()
                }
                bus.send("Hello")
                receivedString shouldBe "Hello"
            }
            scenario("Object message"){
                data class TestClass(val str: String = "")
                var received = TestClass()
                bus.toObservable().subscribe {
                    when(it){
                        is TestClass -> received = it
                    }
                }
                bus.send(TestClass("Hello"))
                received shouldBe TestClass("Hello")
            }
            scenario("Can check if subs present"){
                bus.toObservable().subscribe {
                    println("Just a normal function bodu")
                }
                bus.hasObservers() shouldBe true
            }
        }
    }
}
