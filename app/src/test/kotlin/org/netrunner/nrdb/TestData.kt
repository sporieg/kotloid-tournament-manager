package org.netrunner.nrdb

object TestData{
    val factions = """{
  "data": [
    {
      "code": "anarch",
      "color": "FF4500",
      "is_mini": false,
      "name": "Anarch",
      "side_code": "runner"
    },
    {
      "code": "criminal",
      "color": "4169E1",
      "is_mini": false,
      "name": "Criminal",
      "side_code": "runner"
    },
    {
      "code": "shaper",
      "color": "32CD32",
      "is_mini": false,
      "name": "Shaper",
      "side_code": "runner"
    },
    {
      "code": "neutral-runner",
      "color": "808080",
      "is_mini": false,
      "name": "Neutral",
      "side_code": "runner"
    },
    {
      "code": "haas-bioroid",
      "color": "8A2BE2",
      "is_mini": false,
      "name": "Haas-Bioroid",
      "side_code": "corp"
    }
  ],
  "total": 5,
  "success": true,
  "version_number": "2.0",
  "last_updated": "2016-07-07T07:57:36+00:00"
}"""
    var cards = """{
  "imageUrlTemplate": "https://netrunnerdb.com/card_image/{code}.png",
  "data": [
    {
      "code": "00005",
      "deck_limit": 1,
      "faction_code": "neutral-corp",
      "flavor": "The Past is the Future.",
      "illustrator": "Sławomir Maniak",
      "influence_limit": null,
      "keywords": "Megacorp",
      "minimum_deck_size": 30,
      "pack_code": "draft",
      "position": 5,
      "quantity": 1,
      "side_code": "corp",
      "text": "Draft format only.\nYou can use agendas from all factions in this deck.",
      "title": "The Shadow: Pulling the Strings",
      "type_code": "identity",
      "uniqueness": false
    }
  ],
  "total": 1,
  "success": true,
  "version_number": "2.0",
  "last_updated": "2017-03-23T06:51:18+00:00"
}"""
}