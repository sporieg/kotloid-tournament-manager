package org.netrunner.nrdb

import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import io.kotlintest.forAll
import io.kotlintest.forAtLeastOne
import io.kotlintest.forSome
import io.kotlintest.matchers.*
import io.kotlintest.specs.FeatureSpec
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory


typealias FactionAdapter = JsonAdapter<FactionList>
typealias CardAdapter = JsonAdapter<CardList>

class DeserializationSpec: FeatureSpec(){
    init{
        feature("Can deserialize from NRDB") {
            val moshi = Moshi.Builder().build()!!
            val factionAdapter: FactionAdapter = moshi.adapter(FactionList::class.java)
            val identityAdapter: CardAdapter = moshi.adapter(CardList::class.java)
            scenario("Can handle factions"){
                val fromServer = factionAdapter.fromJson(TestData.factions)
                val f: FactionList = fromServer!!
                f.data shouldNot beEmpty()
                forAll(f.data){
                    it.code.length shouldBe beGreaterThan(1)
                    it.color.length shouldBe beGreaterThan(1)
                }
            }
            scenario("Can deserilize mixed types of cards"){
                val i: CardList = identityAdapter.fromJson(TestData.cards)!!
                i.data shouldNot beEmpty()
                forAtLeastOne(i.data){
                    it.side_code shouldBe "corp"
                }
            }
        }
        feature("***Integration*** Can deserialize from NRDN"){
            val retrofit  = Retrofit.Builder()
                    .baseUrl(NrdbApi.NRDB_API_URL)
                    .addConverterFactory(MoshiConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build()
            /*
            * Note:  Had to import the NRDB cert onto the jvm, I was getting
            * sun.security.validator.ValidatorException: PKIX path building failed: sun.security.provider.certpath.SunCertPathBuilderException: unable to find valid certification path to requested target
            *
            * To fix this I followed steps from:
            * http://stackoverflow.com/questions/21076179/pkix-path-building-failed-and-unable-to-find-valid-certification-path-to-requ
            * */
            val api = retrofit.create(NrdbApi::class.java)
            scenario("Can handle factions"){
                api.getFactions().blockingSubscribe {
                    val f = it

                    f shouldNotBe null
                    f.data shouldNot beEmpty()
                    forAll(f.data){
                        it.code.length shouldBe beGreaterThan(1)
                        it.color.length shouldBe beGreaterThan(1)
                    }
                    forAtLeastOne(f.data){
                        it.is_mini shouldBe true
                    }
                }
            }

            scenario("Can deserilize mixed types of cards"){
                api.getCards().blockingSubscribe {
                    val i = it
                    i.data shouldNot beEmpty()
                    forSome(i.data){
                        it.side_code shouldBe "corp"
                    }
                    forSome(i.data){
                        it.type_code shouldBe "identity"
                    }
                }
            }
        }
    }
}