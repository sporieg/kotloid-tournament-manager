package org.netrunner.nrdb

import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.netrunner.rx.ReplaceFactions
import org.netrunner.rx.ReplaceIdentities
import org.netrunner.rx.RxBus
import org.netrunner.tournament.Faction
import org.netrunner.tournament.Identity
import retrofit2.http.GET


interface NrdbApi{
    companion object{
        val NRDB_API_URL = "https://netrunnerdb.com"
    }
    @GET("/api/2.0/public/factions")
    fun getFactions(): Observable<FactionList>
    @GET("/api/2.0/public/cards")
    fun getCards(): Observable<CardList>
}

class Importer(val api: NrdbApi, val bus: RxBus) {
    init{
        bus.toObservable().subscribe {
            when (it) {
                is ImportFromNrdb -> import()
            }
        }
    }
    fun import() {
        api.getFactions().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe {
            val msg = ReplaceFactions(it.data.map { Faction(id = it.code, color = it.color, isMini = it.is_mini, name = it.name, sideCode = it.side_code) })
            bus.send(msg)
        }
        api.getCards().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe {
            val identities = it.data.filter { it.type_code == "identity" }.map { Identity(it.code, it.title, it.faction_code) }.toList()
            bus.send(ReplaceIdentities(identities))
        }
    }
}

data class FactionList(val data: List<ApiFaction>)
data class ApiFaction(val code: String, val color: String, val is_mini: Boolean, val name: String, val side_code: String)
data class CardList(val imageUrlTemplate: String, val data: List<IdentityCard>, val total: Int, val success: Boolean, val version_number: String, val last_updated: String)
data class IdentityCard(val code: String, val faction_code: String, val side_code: String, val type_code: String, val title: String)