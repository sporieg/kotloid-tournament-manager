package org.netrunner

import org.netrunner.data.IFactionDataManager
import org.netrunner.data.ITournamentDataManager
import org.netrunner.data.TournamentHeaderInfo
import org.netrunner.tournament.*
import org.netrunner.tournament.swiss.*
import java.util.*
import javax.inject.Scope

/**
 * For globals acroos the app.
 */
@Scope
@kotlin.annotation.Retention
annotation class ApplicationScope

/**
 * Shared across a group of Activity/Fragments.
 * eg: Tournaments, MainNav
 */
@Scope
@kotlin.annotation.Retention
annotation class ControllerScope

/**
 * Needed at a smaller scope than Controller, cannot really be shared.
 */
@Scope
@kotlin.annotation.Retention
annotation class ServiceScope


fun sampleData(dataManager: ITournamentDataManager, factionManager: IFactionDataManager) {
    val t = dataManager.newTournament()
    fun runner() = factionManager.aRunner()
    fun corp() = factionManager.aCorp()

    fun CorpSplit(p1: Player, p2: Player): SwissPlayerPairing {
        val game1 = SwissGame(p1.id, p2.id, Points.Win.points, Points.Loss.points)
        val game2 = SwissGame(p2.id, p1.id, Points.Win.points, Points.Loss.points)
        val match = SwissMatch(game1, game2)
        return SwissPlayerPairing(p1.id, p2.id, match = match)
    }

    fun RunnerSplit(p1: Player, p2: Player): SwissPlayerPairing {
        val game1 = SwissGame(p1.id, p2.id, Points.Loss.points, Points.Win.points)
        val game2 = SwissGame(p2.id, p1.id, Points.Loss.points, Points.Win.points)
        val match = SwissMatch(game1, game2)
        return SwissPlayerPairing(p1.id, p2.id, match = match)
    }

    fun Player1Sweep(p1: Player, p2: Player): SwissPlayerPairing {
        val game1 = SwissGame(p1.id, p2.id, Points.Win.points, Points.Loss.points)
        val game2 = SwissGame(p2.id, p1.id, Points.Loss.points, Points.Win.points)
        val match = SwissMatch(game1, game2)
        return SwissPlayerPairing(p1.id, p2.id, match = match)
    }

    fun Player2Sweep(p1: Player, p2: Player): SwissPlayerPairing {
        val game1 = SwissGame(p1.id, p2.id, Points.Loss.points, Points.Win.points)
        val game2 = SwissGame(p2.id, p1.id, Points.Win.points, Points.Loss.points)
        val match = SwissMatch(game1, game2)
        return SwissPlayerPairing(p1.id, p2.id, match = match)
    }

    fun SwissRound.sync(): SwissRound {
        pairings.forEach {
            it.round = num
        }
        return this
    }
    t.tournament.let {
        val zero = it.addPlayer("Zero", corp(), runner())
        val one = it.addPlayer("One", corp(), runner())
        val two = it.addPlayer("Two", corp(), runner())
        val three = it.addPlayer("Three", corp(), runner())
        val four = it.addPlayer("Four", corp(), runner())
        val five = it.addPlayer("Five", corp(), runner())
        val b2 = it.addPlayer("2B", corp(), runner())
        val s9 = it.addPlayer("9S", corp(), runner())
        val A2 = it.addPlayer("A2", corp(), runner())
        it.addRound(SwissRound(1, listOf(
                SwissBye(s9),
                CorpSplit(zero, one),
                Player2Sweep(two, three),
                RunnerSplit(four, five),
                Player1Sweep(A2, b2)
        )).sync())
        it.addRound(SwissRound(2, listOf(
                SwissBye(b2),
                Player1Sweep(zero, three),
                CorpSplit(two, five),
                RunnerSplit(four, one),
                Player1Sweep(A2, s9)
        )).sync())
        it.addRound()
        val gen = SwissRoundGenerator(it)
        val r = gen.pairRound(3)
        it.assignPairings(r)
    }
    dataManager.save(t)
    dataManager.updateHeader(TournamentHeaderInfo(t.id, Date(), "My House", "Kaine"))
}
