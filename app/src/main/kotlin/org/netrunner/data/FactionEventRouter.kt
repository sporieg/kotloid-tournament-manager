package org.netrunner.data

import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import org.netrunner.rx.*

class FactionEventRouter(val fm: IFactionDataManager, val bus: RxBus){
    lateinit var sub: Disposable
    init {
        sub()
    }
    fun sub(){
        sub = bus.toObservable().subscribe{
            when(it){
                is ReplaceFactions -> {
                    fm.save(it.factions)
                    bus.send(FactionsUpdated)
                }
                is ReplaceIdentities -> {
                    val ids = it.identities
                    Observable.create<Unit>{
                        fm.saveIdentities(ids)
                    }.subscribe{
                        bus.send(IdentitiesUpdated)
                    }
                }
                is CreateFaction -> {
                    fm.add(it.f)
                    bus.send(FactionCreated(it.f))
                }
                is DeleteFaction -> {
                    fm.remove(it.f)
                    bus.send(FactionDeleted(it.f))
                }
                is UpdateFaction -> {
                    fm.update(it.originalId, it.f)
                    bus.send(FactionUpdated(it.f))
                }
                is CreateIdentity -> {
                    fm.add(it.i)
                    bus.send(IdentityCreated(it.i))
                }
                is UpdateIdentity -> {
                    fm.update(it.i)
                    bus.send(IdentityUpdated(it.i))
                }
                is DeleteIdentity -> {
                    fm.remove(it.i)
                    bus.send(IdentityDeleted(it.i))
                }
            }
        }}
}