package org.netrunner.data

import org.netrunner.tournament.Faction
import org.netrunner.tournament.FactionId
import org.netrunner.tournament.Identity
import org.netrunner.tournament.swiss.SwissTournament
import java.util.*

/**
 * Basic data stubs to start, define the API of data sharing, and fill in UI pieces first.
 */

data class TournamentData(val id: UUID, val tournament: SwissTournament)
data class TournamentHeaderInfo(val id: UUID, var date: Date, var location: String, var organizer: String)
data class IdentityFaction(val faction: Faction, val identity: Identity)

interface ITournamentDataManager {
    fun newTournament(): TournamentData
    fun save(td: TournamentData, callback: (TournamentData) -> Unit = {})
    fun updateHeader(h: TournamentHeaderInfo, callback: (TournamentHeaderInfo) -> Unit = {})
    /**
     * Produce the current tournament, or create and return it if none exists.
     */
    fun currentTournament(): TournamentData
    fun tournament(id: UUID): TournamentData
    fun tournamentHeaders(): List<TournamentHeaderInfo>
    fun tournamentHeader(id: UUID): TournamentHeaderInfo
}


interface IFactionDataManager {
    fun save(factions: List<Faction>)
    fun add(f: Faction)
    fun remove(f: Faction)
    fun update(originalId: FactionId, f: Faction)
    fun add(i: Identity)
    fun remove(i: Identity)
    fun update(i: Identity)
    fun saveIdentities(identities: List<Identity>)
    fun factions(): List<Faction>
    fun factionFor(i: Identity): Faction
    fun identities(): List<Identity>
    fun identity(key: String): Identity
    fun corpFactions(): List<Faction>
    fun runnerFactions(): List<Faction>
    fun identityFactions(): List<IdentityFaction>
    fun identityFactionsRunners(): List<IdentityFaction>
    fun identityFactionsCorps(): List<IdentityFaction>
    /*
     Do we have enough data to run a tournament?
     */
    fun isValid(): Boolean
    fun aCorp(): Identity
    fun aRunner(): Identity
}
