package org.netrunner.data.Paper
import android.util.Log
import io.paperdb.Book
import org.netrunner.data.ITournamentDataManager
import org.netrunner.data.TournamentData
import org.netrunner.data.TournamentHeaderInfo
import org.netrunner.tournament.swiss.SwissTournament
import java.util.*


class PaperTournamentManager(val tournamentHeaderBook: Book, val tournamentsBook: Book): ITournamentDataManager{
    companion object {
        private val CurrentTournament = "CurrentTournamentId"
    }

    //In memory cache of the paper storage.
    val headers: MutableList<TournamentHeaderInfo> by lazy {
        tournamentHeaderBook.allKeys.let {
            it.map {
                tournamentHeaderBook.read<TournamentHeaderInfo>(it)
            }.toMutableList()
        }
    }

    override fun newTournament(): TournamentData {
        val t = TournamentData(UUID.randomUUID(), SwissTournament())
        save(t)
        tournamentsBook.write(CurrentTournament, t.id)
        val header = TournamentHeaderInfo(t.id, Date(), "", "")
        headers.add(header)
        tournamentHeaderBook.write("$t.id", header)
        return t
    }

    override fun save(td: TournamentData, callback: (TournamentData)-> Unit){
        Log.d("TournamentDataManager","save $td")
        val id = "${td.id}"
        tournamentsBook.write(id, td)
        Log.d("TournamentDataManager","saved $td")
        callback(td)
    }
    override fun tournamentHeaders(): List<TournamentHeaderInfo> = headers
    override fun tournamentHeader(id: UUID): TournamentHeaderInfo = headers.first{it.id == id}

    override fun updateHeader(h: TournamentHeaderInfo, callback: (TournamentHeaderInfo) -> Unit) {
        val idx = headers.indexOfFirst{it.id == h.id}
        headers[idx] = h
        tournamentHeaderBook.write("$h.id", h)
        callback(h)
    }

    override fun currentTournament(): TournamentData {
        val id: UUID = tournamentsBook.read(CurrentTournament) ?: newTournament().id
        return tournament(id)
    }
    override fun tournament(id: UUID): TournamentData {
        Log.d("TournamentDataManager","finding $id")
        val sId = "$id"
        return  tournamentsBook.read(sId)
    }
}


