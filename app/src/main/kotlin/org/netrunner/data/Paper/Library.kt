package org.netrunner.data.Paper

import io.paperdb.Book
import io.paperdb.Paper

object Library{
    fun tournamentsBook(): Book = Paper.book("TournamentData")
    fun tournamentHeaderBook(): Book = Paper.book("TournamentHeader")
    fun factionBook(): Book = Paper.book("Factions")
    fun identityBook(): Book = Paper.book("identities")
}