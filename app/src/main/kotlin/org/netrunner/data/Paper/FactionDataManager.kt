package org.netrunner.data.Paper

import io.paperdb.Book
import org.netrunner.data.IFactionDataManager
import org.netrunner.data.IdentityFaction
import org.netrunner.tournament.Faction
import org.netrunner.tournament.FactionId
import org.netrunner.tournament.Identity
import java.util.*


class FactionDataManager(val factionBook: Book, val identityBook: Book): IFactionDataManager {
    val IdentityKey = "identities"
    val FactionKey = "Factions"
    private var factions: MutableList<Faction> = factionBook.read(FactionKey) ?: mutableListOf()
    private var identities: MutableList<Identity> = identityBook.read(IdentityKey) ?: mutableListOf()

    override fun add(f: Faction) {
        factions.add(f)
        save(factions)
    }
    override fun remove(f: Faction) {
        factions.remove(f)
        save(factions)
    }
    override fun update(originalId: FactionId, f: Faction){
        factions[factions.indexOfFirst { it.id == originalId }] = f
        save(factions)
    }
    override fun save(factions: List<Faction>){
        factionBook.write(FactionKey, factions)
        this.factions = factions.toMutableList()
    }
    override fun saveIdentities(identities: List<Identity>){
        identityBook.write(IdentityKey, identities)
        this.identities = identities.toMutableList()
    }

    override fun add(i: Identity) {
        identities.add(i)
        saveIdentities(identities)
    }

    override fun remove(i: Identity) {
        identities.remove(i)
        saveIdentities(identities)
    }

    override fun update(i: Identity) {
        identities[identities.indexOf(i)] = i
        saveIdentities(identities)
    }
    override fun factions(): List<Faction> = factions
    override fun factionFor(i: Identity) = factions.firstOrNull{it.id == i.factionId } ?: Faction()
    override fun identities(): List<Identity> = identities
    override fun identity(key: String) = identities.firstOrNull { it.code == key } ?: Identity()
    override fun corpFactions() = factions().filter { it.sideCode == "corp" }
    override fun runnerFactions() = factions().filter { it.sideCode == "runner" }
    override fun identityFactions() = identities.map { IdentityFaction(factionFor(it), it) }
    override fun identityFactionsRunners() = identityFactions().filter { (f, _) -> runnerFactions().any { it == f }}
    override fun identityFactionsCorps() = identityFactions().filter { (f, _) -> corpFactions().any { it == f }}

    private val random = Random()
    override fun aCorp(): Identity = identityFactionsCorps()[random.nextInt(identityFactionsCorps().count())].identity
    override fun aRunner(): Identity = identityFactionsRunners()[random.nextInt(identityFactionsRunners().count())].identity
    override fun isValid(): Boolean = identityFactionsCorps().any() && identityFactionsRunners().any()
}