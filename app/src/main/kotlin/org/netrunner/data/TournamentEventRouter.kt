package org.netrunner.data

import io.reactivex.disposables.Disposable
import org.netrunner.rx.*
import org.netrunner.tournament.swiss.SwissRoundGenerator

class TournamentEventRouter(val tdm: ITournamentDataManager, val fdm: IFactionDataManager, val bus: RxBus){
    lateinit var sub: Disposable
    init {
        sub()
    }

    fun handleTourneyEvent(event: TournamentEvent){
        val td = tdm.tournament(event.tournamentId)
        val t = td.tournament
        when(event){
            is AddPlayer -> {
                val p =t.addPlayer(event.nickName, event.corpId, event.runnerId)
                tdm.save(td){
                    bus.send(PlayerAdded(it.id, p))
                }
            }
            is UpdatePlayer -> {
                t.updatePlayer(event.playerUpdated){ p ->
                    p.corp = fdm.identity(event.corpCode)
                    p.runner = fdm.identity(event.runnerCode)
                    p.nickName = event.nickName
                    p.dropped = !event.active
                    tdm.save(td){
                        bus.send(PlayerUpdated(it.id, p))
                    }
                }
            }
            is DeletePlayer -> {
                t.player(event.playerNum)?.let { p ->
                    t.removePlayer(p)
                    tdm.save(td){
                        bus.send(PlayerDeleted(it.id, p))
                    }
                }
            }
            is AddRound -> {
                t.addRound()
                tdm.save(td){
                    bus.send(RoundAdded(event.tournamentId))
                }
            }
            is UpdateHeader -> {
                val header = tdm.tournamentHeader(event.tournamentId)
                header.location = event.location
                header.organizer = event.organizer
                tdm.updateHeader(header){
                    bus.send(HeaderUpdated(event.tournamentId))
                }
            }
            is DropLast -> {
                t.dropLast()
                tdm.save(td){
                    bus.send(RoundRemoved(td.id))
                }
            }
            is PairRound -> {
                val gen = SwissRoundGenerator(t)
                t.assignPairings(gen.pairRound(event.swissRound.num))
                tdm.save(td){
                    bus.send(RoundPaired(t.round(event.swissRound.num), td.id))
                }
            }
            is ReportSwissRound -> {
                val g1 = event.g1
                val g2 = event.g2
                val p1 = t.players.first { it.id == g1.corpPlayerId }.id
                val p2 = t.players.first { it.id == g1.runnerPlayerId }.id
                val round = t.round(event.round)
                val pairing = t.round(event.round).between(p1, p2)
                val match = pairing.match
                match.game1.setPointsFor(p1, g1.pointsFor(p1))
                match.game1.setPointsFor(p2, g1.pointsFor(p2))
                match.game2.setPointsFor(p1, g2.pointsFor(p1))
                match.game2.setPointsFor(p2, g2.pointsFor(p2))
                tdm.save(td){
                    bus.send(RoundReported(td.id, round))
                }
            }

        }
    }
    fun sub(){
        sub = bus.toObservable().subscribe{
            when(it){
                is TournamentEvent -> handleTourneyEvent(it)
            }
        }
    }
}