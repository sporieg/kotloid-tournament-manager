package org.netrunner.data

import android.util.Log
import org.netrunner.tournament.Faction
import org.netrunner.tournament.FactionId
import org.netrunner.tournament.Identity
import org.netrunner.tournament.swiss.SwissTournament
import java.util.*

class InMemoryDataManager: ITournamentDataManager {
    val tournament: HashMap<UUID, SwissTournament> = hashMapOf()
    val headers: HashMap<UUID, TournamentHeaderInfo> = hashMapOf()
    var currentId: UUID? = null

    init {
        Log.v("InMemoryDataManager", "Creating InMemoryDataManager")
    }

    override fun newTournament(): TournamentData {
        val t = TournamentData(UUID.randomUUID(), SwissTournament())
        save(t){
            currentId = t.id
        }
        return t
    }

    override fun save(td: TournamentData, callback: (TournamentData) -> Unit) {
        tournament[td.id] = td.tournament
        callback(td)
    }
    override fun updateHeader(h: TournamentHeaderInfo, callback: (TournamentHeaderInfo) -> Unit) {
        headers[h.id] = h
        callback(h)
    }

    override fun tournamentHeaders(): List<TournamentHeaderInfo> = headers.map { it.value }
    override fun tournamentHeader(id: UUID): TournamentHeaderInfo = headers[id]!!
    override fun currentTournament(): TournamentData {
        if(currentId == null){
            return newTournament()
        }
        return tournament(currentId!!)
    }
    override fun tournament(id: UUID): TournamentData {
        val t = tournament[id]!!
        return TournamentData(id, t)
    }
}

class InMemoryFactionDataManager : IFactionDataManager{

    val savedFactions: MutableList<Faction> = mutableListOf()
    val savedIdentities: MutableList<Identity> = mutableListOf()

    override fun add(f: Faction) {
        savedFactions.add(f)
    }

    override fun remove(f: Faction) {
        savedFactions.remove(f)
    }

    override fun update(originalId: FactionId, f: Faction){
        savedFactions[savedFactions.indexOfFirst { it.id == originalId }] = f
    }
    override fun save(factions: List<Faction>) {
        savedFactions.clear()
        savedFactions.addAll(factions)
    }
    override fun saveIdentities(identities: List<Identity>) {
        savedIdentities.clear()
        savedIdentities.addAll(identities)
    }

    override fun add(i: Identity) {
        savedIdentities.add(i)
    }

    override fun remove(i: Identity) {
        savedIdentities.remove(i)
    }

    override fun update(i: Identity) {
        savedIdentities[savedIdentities.indexOf(i)] = i
    }
    override fun factions(): List<Faction> = savedFactions
    override fun identities(): List<Identity> = savedIdentities
    override fun factionFor(i: Identity): Faction = savedFactions.first { it.id == i.factionId }
    override fun identity(key: String): Identity = savedIdentities.first { it.code == key }
    override fun corpFactions(): List<Faction> = savedFactions.filter { it.sideCode == "corp" }
    override fun runnerFactions(): List<Faction> = savedFactions.filter { it.sideCode == "runner" }
    override fun identityFactions() = savedIdentities.map { IdentityFaction(factionFor(it), it) }
    override fun identityFactionsRunners() = identityFactions().filter { (f, _) -> runnerFactions().any { it == f }}
    override fun identityFactionsCorps() = identityFactions().filter { (f, _) -> corpFactions().any { it == f }}
    override fun aCorp(): Identity = identityFactionsCorps().first().identity
    override fun aRunner(): Identity = identityFactionsRunners().first().identity
    override fun isValid(): Boolean = identityFactionsCorps().any() && identityFactionsRunners().any()
}