package org.netrunner.rx

import org.netrunner.tournament.*
import org.netrunner.tournament.swiss.SwissRound
import org.netrunner.tournament.swiss.WinType
import java.util.*

interface Event

abstract class TournamentEvent(val tournamentId: UUID): Event

class AddPlayer(val nickName: String, val corpId: Identity, val runnerId: Identity, id: UUID): TournamentEvent(id)
class DeletePlayer(val playerNum: Int, tournamentId: UUID): TournamentEvent(tournamentId)
class UpdatePlayer(tournamentId: UUID, val playerUpdated: PlayerId, val nickName: String, val corpCode: String, val runnerCode: String, val active: Boolean = true): TournamentEvent(tournamentId)


abstract class TournamentUpdated(val tournamentId: UUID)
class PlayerDeleted(tournamentId: UUID, val player: Player): TournamentUpdated(tournamentId)
class PlayerAdded(tournamentId: UUID, val player: Player): TournamentUpdated(tournamentId)
class PlayerUpdated(tournamentId: UUID, val player: Player): TournamentUpdated(tournamentId)
class HeaderUpdated(tournamentId: UUID): TournamentUpdated(tournamentId)

class UpdateHeader(id: UUID, var location: String, var organizer: String): TournamentEvent(id)
class AddRound(id: UUID): TournamentEvent(id)
class RoundAdded(id: UUID): TournamentUpdated(id)
class DropLast(id: UUID):TournamentEvent(id)
class RoundRemoved(id: UUID):TournamentUpdated(id)
class PairRound(val swissRound: SwissRound, id: UUID): TournamentEvent(id)
class RoundPaired(val swissRound: SwissRound, id: UUID): TournamentUpdated(id)
data class GameReport(val runnerPlayerId: Int, val corpPlayerId: Int, val runnerPoints: Int, val corpPoints: Int, val winType: WinType){
    fun pointsFor(id: Int): Int{
        when(id){
            runnerPlayerId -> return runnerPoints
            corpPlayerId -> return corpPoints
            else -> return 0
        }
    }
}
class ReportSwissRound(id: UUID, val round: Int, val g1: GameReport, val g2: GameReport): TournamentEvent(id)
class RoundReported(id: UUID, val swissRound: SwissRound): TournamentUpdated(id)

data class ReplaceFactions(val factions: List<Faction>)
data class ReplaceIdentities(val identities: List<Identity>)
object FactionsUpdated
object IdentitiesUpdated
data class CreateFaction(val f: Faction = Faction())
data class FactionCreated(val f: Faction)
data class DeleteFaction(val f: Faction)
data class FactionDeleted(val f: Faction)
data class UpdateFaction(val originalId: FactionId, val f: Faction)
data class FactionUpdated(val f: Faction)
data class CreateIdentity(val i: Identity)
data class IdentityCreated(val i: Identity)
data class UpdateIdentity(val i: Identity)
data class IdentityUpdated(val i: Identity)
data class DeleteIdentity(val i: Identity)
data class IdentityDeleted(val i: Identity)