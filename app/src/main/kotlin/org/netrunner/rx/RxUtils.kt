package org.netrunner.rx

import android.databinding.Observable.OnPropertyChangedCallback
import android.databinding.ObservableInt
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.FlowableEmitter
import io.reactivex.Observable
import io.reactivex.disposables.Disposable


object DefaultSub: Disposable {
    override fun isDisposed(): Boolean = true
    override fun dispose() {}
}

object RxUtils {
    fun ObservableInt.toObservable(): Observable<Int> {
        val observableField = this
        return Flowable.create({ asyncEmitter: FlowableEmitter<Int> ->
            val callback = object : OnPropertyChangedCallback() {
                override fun onPropertyChanged(dataBindingObservable: android.databinding.Observable, propertyId: Int) {
                    if (dataBindingObservable === observableField) {
                        asyncEmitter.onNext(observableField.get())
                    }
                }
            }
            observableField.addOnPropertyChangedCallback(callback)
            asyncEmitter.setCancellable({ this.removeOnPropertyChangedCallback(callback) })
        }, BackpressureStrategy.LATEST).toObservable()
    }
}