package org.netrunner.rx

import android.util.Log
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

class RxBus {
    private val bus = PublishSubject.create<Any>()
    init{
        Log.d("RxBus", "Created event bus.")
    }

    fun send(o: Any) {
        Log.v("RxBus", "Logging Event ${o::class}")
        bus.onNext(o)
    }

    fun toObservable(): Observable<Any> {
        Log.v("RxBus", "Grabbing Bus $this")
        return bus
    }

    fun hasObservers(): Boolean =  bus.hasObservers()

}