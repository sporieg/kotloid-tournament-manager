package org.netrunner.kotloid

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat.*
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import kotlinx.android.synthetic.main.activity_main_nav.*
import kotlinx.android.synthetic.main.app_bar_main_nav.*
import org.netrunner.App
import org.netrunner.routing.Navigation
import org.netrunner.routing.SimpleRouter
import javax.inject.Inject


data class MainNavState(var menu: Int)


class MainNav : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {
    @Inject
    lateinit var router: SimpleRouter
    lateinit var component: MainNavComponent
    companion object{
        var state: MainNavState = MainNavState(R.id.nav_tournament_history)
        fun BaseKotloidFragment.component(): MainNavComponent = (activity as MainNav).component
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        component = App.appComponent
                .newMainNavComponent(MainNavModule(this))
        component.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_nav)
        setSupportActionBar(toolbar)
        val toggle = ActionBarDrawerToggle(this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()
        nav_view.setNavigationItemSelectedListener(this)
    }

    override fun onResume() {
        super.onResume()
        route(router[state.menu])
    }

    override fun onPause(){
        super.onPause()
    }

    override fun onBackPressed() {
        val drawer = drawer_layout
        if (drawer.isDrawerOpen(START)) {
            drawer.closeDrawer(START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main_nav, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        val f = router[id]
        return route(f)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        val route = router[item.itemId]
        return route(route)
    }

    fun route(route: Navigation): Boolean{
        when(route){
            is Navigation.Routable -> {
                toolbar.setTitle(route.title)
                supportFragmentManager.beginTransaction()
                        .replace(R.id.content_frame, route.fragment)
                        .commit()
                drawer_layout.closeDrawer(START)
                return true
            }
            is Navigation.ActivityIntent -> {
                val i = Intent(this, route.clazz)
                i.putExtra(RunTournamentActivity.TOURNAMENT_ID_ARGUMENT, route.id)
                startActivity(i)
                return true
            }
            is Navigation.UnRoutable -> {
                drawer_layout.closeDrawer(START)
                return false
            }
            else -> {
                Log.e("MainNav", "Somehow got something that was not even a Fragment Navigation")
                drawer_layout.closeDrawer(START)
                return false
            }
        }
    }
}
