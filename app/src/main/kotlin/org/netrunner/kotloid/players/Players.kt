package org.netrunner.kotloid.players


import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.fragment_players.view.*
import org.netrunner.data.IFactionDataManager
import org.netrunner.data.TournamentData
import org.netrunner.kotloid.BaseKotloidFragment
import org.netrunner.kotloid.RunTournamentActivity.Companion.component
import org.netrunner.kotloid.databinding.FragmentPlayersBinding
import org.netrunner.rx.DefaultSub
import org.netrunner.rx.PlayerAdded
import org.netrunner.rx.RxBus
import javax.inject.Inject

/**
 * A fragment with a Google +1 button.
 */
class Players: BaseKotloidFragment() {
    @Inject
    lateinit var factionManager: IFactionDataManager
    @Inject
    override lateinit var bus: RxBus
    /*
    SwissTournament data is injected at the start, and updated via RX.
     */
    @Inject
    lateinit var td: TournamentData
    @Inject
    lateinit var actBus: PublishSubject<TournamentData>
    var actSub: Disposable = DefaultSub
    init {
        setHasOptionsMenu(true)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        component().inject(this)
        actSub = actBus.subscribe{update(it)}
        super.onCreate(savedInstanceState)
    }

    override fun onDestroy() {
        super.onDestroy()
        if(!actSub.isDisposed) actSub.dispose()
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = SetupViewBindings(inflater, container)
        return attachAdapters(view)
    }

    private fun SetupViewBindings(inflater: LayoutInflater?, container: ViewGroup?): View {
        val b = FragmentPlayersBinding.inflate(inflater, container, false)
        return b.root
    }

    private fun attachAdapters(view: View): View {
        view.playerList.adapter = PlayerListAdapter(view.context, td.tournament.players, td.id, this)
        return view
    }

    override fun handleEvent(e: Any){
        when(e){
            is PlayerAdded -> {
                val p = e.player
                val vm = p.toEditPlayerVm(td)
                val edit = Intent(activity, EditPlayerActivity::class.java)
                edit.putExtra(EditPlayerActivity.Tag, vm)
                startActivity(edit)
            }
        }
    }

    fun update(tournamentData: TournamentData){
        td = tournamentData
        view?.let { attachAdapters(it)}
    }
}

