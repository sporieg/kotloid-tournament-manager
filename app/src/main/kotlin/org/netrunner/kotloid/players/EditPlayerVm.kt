package org.netrunner.kotloid.players

import android.os.Parcel
import android.os.Parcelable
import org.netrunner.kotloid.IdentityVm
import java.util.*

data class EditPlayerVm(val id: Int = 0, val tournamentId: UUID, var nickName: String = "", var active: Boolean = true, var hasPlayed: Boolean = false, var corp: IdentityVm, var runner: IdentityVm) : Parcelable {
    companion object {
        @Suppress("unused")
        @JvmField val CREATOR: Parcelable.Creator<EditPlayerVm> = object : Parcelable.Creator<EditPlayerVm> {
            override fun createFromParcel(source: Parcel): EditPlayerVm = EditPlayerVm(source)
            override fun newArray(size: Int): Array<EditPlayerVm?> = arrayOfNulls(size)
        }
    }

    constructor(source: Parcel) : this(source.readInt(), source.readSerializable() as UUID, source.readString(), 1.equals(source.readInt()), 1.equals(source.readInt()), source.readParcelable<IdentityVm>(IdentityVm::class.java.classLoader), source.readParcelable<IdentityVm>(IdentityVm::class.java.classLoader))

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel?, flags: Int) {
        dest?.writeInt(id)
        dest?.writeSerializable(tournamentId)
        dest?.writeString(nickName)
        dest?.writeInt((if (active) 1 else 0))
        dest?.writeInt((if (hasPlayed) 1 else 0))
        dest?.writeParcelable(corp, 0)
        dest?.writeParcelable(runner, 0)
    }
}