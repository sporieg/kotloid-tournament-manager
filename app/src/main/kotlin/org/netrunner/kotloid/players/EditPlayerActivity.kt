package org.netrunner.kotloid.players

import android.app.AlertDialog
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.Spinner
import android.widget.Toast
import dagger.Subcomponent
import kotlinx.android.synthetic.main.activity_edit_player.*
import kotlinx.android.synthetic.main.edit_player.view.*
import org.netrunner.App
import org.netrunner.ControllerScope
import org.netrunner.data.IFactionDataManager
import org.netrunner.kotloid.*
import org.netrunner.kotloid.RunTournamentActivity.Companion.TOURNAMENT_ID_ARGUMENT
import org.netrunner.kotloid.databinding.ActivityEditPlayerBinding
import org.netrunner.rx.DeletePlayer
import org.netrunner.rx.RxBus
import javax.inject.Inject

@ControllerScope
@Subcomponent
interface EditPlayerComponent{
    fun inject(editPlayer: EditPlayerActivity)
}

class EditPlayerActivity: DataActivity<EditPlayerVm>() {
    companion object{
        val Tag = "edit_player"
    }
    @Inject
    lateinit var factions: IFactionDataManager
    @Inject
    lateinit var bus: RxBus

    override lateinit var model: EditPlayerVm
    lateinit var component: EditPlayerComponent
    var shouldSave = true

    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        component = App.appComponent.newEditPlayerComponent()
        component.inject(this)

        setupActivity(savedInstanceState, Tag)
        val b = DataBindingUtil.setContentView<ActivityEditPlayerBinding>(this, R.layout.activity_edit_player)
        b.editPlayer = model
        attachAdapters(b.root)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.activity_edit_player_menu, menu)
        if(model.hasPlayed) menu?.removeItem(R.id.delete_player)
        return true
    }

    override fun getParentActivityIntent(): Intent {
        val i = super.getParentActivityIntent()
        i.putExtra(TOURNAMENT_ID_ARGUMENT, model.tournamentId)
        i.putExtra(RunTournamentActivity.START_PAGE_SECTION_ID, R.string.tournament_menu_players)
        return i
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        when(id){
            R.id.delete_player -> {
                AlertDialog.Builder(this)
                        .setTitle(R.string.confirm_delete_player)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setPositiveButton(android.R.string.yes, { _, _ ->
                            shouldSave = false
                            bus.send(DeletePlayer(model.id, model.tournamentId))
                            finish()
                            Toast.makeText(this, R.string.deleted_player, Toast.LENGTH_SHORT).show()
                        })
                        .setNegativeButton(android.R.string.no, null).show()
                return true
            }
            R.id.cancel_action -> {
                shouldSave = false
                finish()
                return true
            }
            R.id.save_player -> {
                finish()
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onPause() {
        super.onPause()
        if(shouldSave) savePlayer(model)
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState?.putParcelable(Tag, model)
    }

    fun attachAdapters(view: View): View {
        //Attach corp/runner adapters.
        val corps = factions.identityFactionsCorps()
        android.R.layout.simple_spinner_dropdown_item
        view.corp_spinner.let {
            val adapter = IdentitySpinnerAdapter(view.context, corps)
            it.adapter = adapter
            val corp_idx = adapter.sortedList.indexOf(model.corp)
            it.setSelection(corp_idx)
            it.listen { s ->
                model.corp = s.selectedItem as IdentityVm
            }

        }
        val runners = factions.identityFactionsRunners()
        view.runner_spinner.let{
            val adapter = IdentitySpinnerAdapter(view.context, runners)
            it.adapter = adapter
            it.setSelection(adapter.getPosition(model.runner))
            it.listen { s ->
                model.runner = s.selectedItem as IdentityVm
            }
        }
        return view
    }

    fun Spinner.listen(onChange: (s: Spinner) -> Unit): Unit{
        val s = this
        this.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {}
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                onChange(s)
            }
        }
    }

    fun savePlayer(model: EditPlayerVm) = bus.send(model.toUpdatePlayer())
}