package org.netrunner.kotloid.players

import android.os.Parcel
import android.os.Parcelable
import org.netrunner.kotloid.IdentityVm

data class PlayerVm(val id: Int = 0, var nickName: String = "", var corp: IdentityVm, var runner: IdentityVm, val dropped: Boolean = false) : Parcelable {
    companion object {
        @Suppress("unused")
        @JvmField val CREATOR: Parcelable.Creator<PlayerVm> = object : Parcelable.Creator<PlayerVm> {
            override fun createFromParcel(source: Parcel): PlayerVm = PlayerVm(source)
            override fun newArray(size: Int): Array<PlayerVm?> = arrayOfNulls(size)
        }
    }

    constructor(source: Parcel) : this(source.readInt(), source.readString(), source.readParcelable<IdentityVm>(IdentityVm::class.java.classLoader), source.readParcelable<IdentityVm>(IdentityVm::class.java.classLoader), 1.equals(source.readInt()))

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel?, flags: Int) {
        dest?.writeInt(id)
        dest?.writeString(nickName)
        dest?.writeParcelable(corp, 0)
        dest?.writeParcelable(runner, 0)
        dest?.writeInt((if (dropped) 1 else 0))
    }
}