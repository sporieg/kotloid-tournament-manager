package org.netrunner.kotloid.players

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import org.netrunner.data.IdentityFaction
import org.netrunner.kotloid.IdentityVm
import org.netrunner.kotloid.R
import org.netrunner.kotloid.databinding.DisplayIdentitySmallBinding
import org.netrunner.kotloid.databinding.SpinnerDropdownItemIdentityBinding
import org.netrunner.kotloid.getSafeViewBindings

class IdentitySpinnerAdapter(ctx: Context, identities: List<IdentityFaction>, val sortedList: List<IdentityVm> = identities.mySort()):
        ArrayAdapter<IdentityVm>(ctx,  R.layout.display_identity_small, sortedList){
    companion object{
        @Suppress("unused")
        fun List<IdentityFaction>.mySort() =
            this.sortedWith(compareBy({it.faction.isMini}, { it.faction.sideCode }, {it.faction.id }))
                    .map { it.identity.toIdentityVm(it.faction) }
    }
    fun getViewBindings(convertView: View?, parent: ViewGroup?) = getSafeViewBindings<DisplayIdentitySmallBinding>(context, R.layout.display_identity_small, convertView, parent)
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val(v, b) = getViewBindings(convertView, parent)
        b.id = getItem(position)
        v.tag = b
        return v
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val(v, b) = getDropDownViewBindings(convertView, parent)
        b.id = getItem(position)
        v.tag = b
        return v
    }
    fun getDropDownViewBindings(convertView: View?, parent: ViewGroup?) = getSafeViewBindings<SpinnerDropdownItemIdentityBinding>(context, R.layout.spinner_dropdown_item_identity, convertView, parent)
}