package org.netrunner.kotloid.players

import org.netrunner.data.TournamentData
import org.netrunner.rx.UpdatePlayer
import org.netrunner.tournament.Player

fun PlayerVm.toEditPlayerVm(td: TournamentData): EditPlayerVm = EditPlayerVm(
        id = id,
        tournamentId = td.id,
        nickName = nickName,
        corp = corp,
        runner = runner,
        active = !dropped,
        hasPlayed = td.tournament.roundHistoryFor(id).any())

fun EditPlayerVm.toUpdatePlayer() = UpdatePlayer(
        tournamentId = tournamentId,
        playerUpdated = id,
        nickName = nickName,
        corpCode = corp.code,
        runnerCode = runner.code,
        active = active)

fun Player.toEditPlayerVm(td: TournamentData) = EditPlayerVm(
        id = id,
        tournamentId = td.id,
        nickName = nickName,
        corp = corp.toIdentityVm(),
        runner = runner.toIdentityVm(),
        active = !dropped,
        hasPlayed = td.tournament.roundHistoryFor(this).any()
)

fun Player.toPlayerVm() = PlayerVm(
        id = id,
        nickName = nickName,
        corp = corp.toIdentityVm(),
        runner = runner.toIdentityVm(),
        dropped = dropped)

