package org.netrunner.kotloid.players

import android.content.Context
import android.content.Intent
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import org.netrunner.App
import org.netrunner.kotloid.IdentityVm
import org.netrunner.kotloid.R
import org.netrunner.kotloid.databinding.PlayerListItemBinding
import org.netrunner.kotloid.getSafeViewBindings
import org.netrunner.tournament.Faction
import org.netrunner.tournament.Identity
import org.netrunner.tournament.Player
import java.util.*

interface PlayerListItemHandler{
    fun openEditPlayer(p: PlayerVm)
}

/*
  This function is just too conveniet to eliminate right now.
 */
fun Identity.toIdentityVm(f: Faction =  App.factionManagerOld.factionFor(this)): IdentityVm  = IdentityVm(this.name, f.color, this.code)

class PlayerListAdapter(ctx: Context, val players: MutableList<Player>, val id: UUID, val parent: Players):
        ArrayAdapter<Player>(ctx, layout, players), PlayerListItemHandler
{
    companion object{
        val layout = R.layout.player_list_item
    }
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val p = getItem(position)
        val (cv, binding) = getSafeBindings(convertView, parent)
        binding.player = p.toPlayerVm()
        binding.handler = this
        binding.executePendingBindings()
        cv.tag = binding
        return cv
    }

    fun getSafeBindings(convertView: View?, parent: ViewGroup?): Pair<View, PlayerListItemBinding> {
        fun check(v: View) = v.tag !is PlayerListItemBinding
        return getSafeViewBindings(context, layout, convertView, parent, ::check)
    }

    override fun openEditPlayer(p: PlayerVm){
        val i = Intent(parent.activity, EditPlayerActivity::class.java)
        i.putExtra(EditPlayerActivity.Tag, p.toEditPlayerVm(parent.td))
        parent.startActivity(i)
    }
}