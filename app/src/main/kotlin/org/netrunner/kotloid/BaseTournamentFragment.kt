package org.netrunner.kotloid

import android.support.v4.app.Fragment
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import org.netrunner.rx.DefaultSub
import org.netrunner.rx.RxBus


abstract class BaseKotloidFragment: Fragment(){
    abstract var bus: RxBus
    //Init to a safe default.
    var sub: Disposable = DefaultSub

    /**
     * Handle an system bus event while the fragment is visible so that data updates while
     * the fragment is active will be handled.
     * Will not be called while the fragment is suspended, each fragment will need to manage its own bus
     * if it wants to handle events while suspended.
     */
    open fun handleEvent(e: Any): Unit{}
    override fun onResume() {
        super.onResume()
        unsubscribe()
        sub = bus.toObservable()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe{handleEvent(it)}
    }
    override fun onPause() {
        super.onPause()
        unsubscribe()
    }

    fun unsubscribe(){
        if(!sub.isDisposed) {
            sub.dispose()
        }
    }
}