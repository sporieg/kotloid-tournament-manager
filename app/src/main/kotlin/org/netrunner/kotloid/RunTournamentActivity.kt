package org.netrunner.kotloid

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import dagger.Module
import dagger.Provides
import dagger.Subcomponent
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.activity_run_tournament.*
import org.netrunner.App
import org.netrunner.ControllerScope
import org.netrunner.data.IFactionDataManager
import org.netrunner.data.ITournamentDataManager
import org.netrunner.data.TournamentData
import org.netrunner.data.TournamentHeaderInfo
import org.netrunner.kotloid.players.Players
import org.netrunner.kotloid.ranking.Ranking
import org.netrunner.kotloid.rounds.Rounds
import org.netrunner.kotloid.timer.RoundTimer
import org.netrunner.kotloid.tournaments.TournamentHeaderActivity
import org.netrunner.kotloid.tournaments.toTournamentHeaderVm
import org.netrunner.rx.AddPlayer
import org.netrunner.rx.AddRound
import org.netrunner.rx.RxBus
import org.netrunner.rx.TournamentUpdated
import org.netrunner.tournament.swiss.SwissTournament
import java.util.*
import javax.inject.Inject


class RunTournamentActivity : AppCompatActivity() {
    lateinit var header: TournamentHeaderInfo
    lateinit var tournamentId: UUID
    val tournamentBus: PublishSubject<TournamentData> = PublishSubject.create<TournamentData>()!!
    var tournamentData: TournamentData = TournamentData(UUID.randomUUID(), SwissTournament())
        set(value){
            field = value
            tournamentBus.onNext(value)
        }
    var sub: Disposable = object: Disposable {
        override fun isDisposed(): Boolean = true
        override fun dispose() {}
    }

    var initialPage = 0
    lateinit var component: RunTournamentComponent
    @Inject
    lateinit var dataManager: ITournamentDataManager
    @Inject
    lateinit var bus: RxBus
    @Inject
    lateinit var factionManager: IFactionDataManager
    @Inject
    lateinit var activity: RunTournamentActivity

    companion object{
        val TOURNAMENT_ID_ARGUMENT = "TournamentId"
        val START_PAGE_SECTION_ID = "PagerStart"
        fun BaseKotloidFragment.component(): RunTournamentComponent = (activity as RunTournamentActivity).component
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        super.onRestoreInstanceState(savedInstanceState)
        initializeTournamentData(savedInstanceState)
    }
    override fun onResume() {
        super.onResume()
        unsubscribe()
        sub = bus.toObservable()
                .observeOn(AndroidSchedulers.mainThread())
                .filter { it is TournamentUpdated }
                .subscribe{
                    tournamentData = dataManager.tournament(tournamentId)
                }
    }

    override fun onDestroy() {
        super.onDestroy()
        unsubscribe()
    }
    fun unsubscribe(){
        if(!sub.isDisposed) {
            sub.dispose()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        component = App.appComponent.newRunTournamentComponent(RunTournamentModule(this))
        component.inject(this)
        initializeTournamentData(savedInstanceState)
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_run_tournament)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        // Set up the ViewPager with the sections adapter.
        container.adapter = SectionsPagerAdapter(supportFragmentManager)
        fun setButtonVisFor(position: Int) {
            val fragment = menuItems[position]
            if(fragment.hasAdd) {
                addAction.show()
                addAction.setOnClickListener { fragment.addAction() }
            } else{
                addAction.hide()
                addAction.setOnClickListener { }
            }
        }
        container.addOnPageChangeListener( object: ViewPager.OnPageChangeListener{
            override fun onPageScrollStateChanged(state: Int) {}
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}
            override fun onPageSelected(position: Int) = setButtonVisFor(position)

        })
        container.setCurrentItem(initialPage, false)
        setButtonVisFor(initialPage)
        tabs.setupWithViewPager(container)
        Log.v("RunTournamentActivity", "Started to run tournament $tournamentId")
    }

    private fun initializeTournamentData(savedInstanceState: Bundle?) {
        when (savedInstanceState) {
            null -> {
                tournamentId = intent.getSerializableExtra(TOURNAMENT_ID_ARGUMENT) as UUID
                initialPage = intent.getIntExtra(START_PAGE_SECTION_ID, R.string.tournament_menu_players).menuIndexOf()
            }
            else -> {
                tournamentId = savedInstanceState.getSerializable(TOURNAMENT_ID_ARGUMENT) as UUID
                initialPage = savedInstanceState.getInt(START_PAGE_SECTION_ID, R.string.tournament_menu_players).menuIndexOf()
            }
        }
        header = dataManager.tournamentHeader(tournamentId)
        tournamentData = dataManager.tournament(tournamentId)
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState?.putSerializable(TOURNAMENT_ID_ARGUMENT, tournamentId)
        outState?.putInt(START_PAGE_SECTION_ID,container.currentItem.toMenuLabel())
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean{
        menuInflater.inflate(R.menu.menu_run_tournament, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        val id = item?.itemId
        when(id){
            R.id.menu_tournament_header -> {
                val i = Intent(this, TournamentHeaderActivity::class.java)
                i.putExtra(TournamentHeaderActivity.Tag, header.toTournamentHeaderVm())
                startActivity(i)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    /**
     * A [FragmentPagerAdapter] that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    data class Section(val label: Int, val builder: ()-> Fragment, val hasAdd: Boolean = false, val addAction: () -> Unit = {}){
        val instance: Fragment by lazy {
            Log.v("RunTournamentActivity", "Creating fragment.")
            builder()
        }
    }
    fun Int.menuIndexOf(): Int = menuItems.indexOfFirst { it.label == this }
    fun Int.toMenuLabel(): Int = menuItems.getOrNull(this)?.label ?: R.string.tournament_menu_players

    val menuItems = listOf(
            Section(R.string.tournament_menu_players, fun(): Fragment = Players(), true, {
                bus.send(AddPlayer("", factionManager.aRunner(), factionManager.aCorp(), tournamentId))
            }),
            Section(R.string.tournament_menu_rounds ,fun(): Fragment = Rounds(), true, {
                AlertDialog.Builder(activity)
                        .setTitle(R.string.confirm_add_round)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setPositiveButton(android.R.string.yes, { _, _ ->
                            bus.send(AddRound(tournamentId))
                            Toast.makeText(activity, R.string.added_round, Toast.LENGTH_SHORT).show()
                        })
                        .setNegativeButton(android.R.string.no, null).show()
            }),
            Section(R.string.tournament_menu_round_timer ,fun(): Fragment = RoundTimer()),
            Section(R.string.tournament_menu_ranking ,fun(): Fragment = Ranking())
    )
    inner class SectionsPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {
        override fun getItem(position: Int): Fragment = menuItems[position].instance
        override fun getCount(): Int = menuItems.count()
        override fun getPageTitle(position: Int): CharSequence? {
            menuItems.getOrNull(position)?.let {
                return resources.getString(it.label)
            }
            return "NA"
        }
    }
}


@Module
class RunTournamentModule(val activity: RunTournamentActivity){
    @Provides
    @ControllerScope
    fun activity(): RunTournamentActivity = activity
    @Provides
    @ControllerScope
    fun context(): Context = activity
    @Provides
    @ControllerScope
    fun tournamentData(): TournamentData = activity.tournamentData
    @Provides
    @ControllerScope
    fun activityBus(): PublishSubject<TournamentData> = activity.tournamentBus
}

@ControllerScope
@Subcomponent(modules =  arrayOf(RunTournamentModule::class))
interface RunTournamentComponent{
    fun inject(runTournament: RunTournamentActivity)
    fun inject(tournamentHeader: TournamentHeaderActivity)
    fun inject(fragment: Players)
    fun inject(fragment: Ranking)
    fun inject(fragment: Rounds)
    fun inject(fragment: RoundTimer)
}