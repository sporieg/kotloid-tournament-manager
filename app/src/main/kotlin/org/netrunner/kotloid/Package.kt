package org.netrunner.kotloid

import android.content.Context
import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.graphics.Color
import android.os.Bundle
import android.os.Parcelable
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup


abstract class DataActivity<T>: AppCompatActivity(){
    abstract var model: T
}

fun <T : Parcelable?> DataActivity<T>.setupActivity(savedInstanceState: Bundle?, tag: String){
    when(savedInstanceState){
        null -> model = intent.getParcelableExtra<T>(tag)
        else -> model = savedInstanceState.getParcelable<T>(tag)
    }
}


fun <T: ViewDataBinding> getSafeViewBindings(ctx: Context, layout: Int, convertView: View?, parent: ViewGroup?, check: (View) -> Boolean = {false}): Pair<View, T> {
    if(convertView == null || check(convertView)){
        val inflater = LayoutInflater.from(ctx)
        val b: T = DataBindingUtil.inflate(inflater, layout, parent, false)
        return Pair(b.root, b)
    }
    @Suppress("UNCHECKED_CAST")
    return Pair(convertView, convertView.tag as T)
}

fun getAndroidColor(id: IdentityVm?): Int {
    return getAndroidColor(id?.color)
}

fun getHexColor(color: Int): String{
    return try{ String.format("%06X", 0xFFFFFF and color)}
        catch (e: Exception){
        "ff3300"
    }
}

fun getAndroidColor(color: String?): Int {
    return try {
        Color.parseColor("#$color")} catch (e: IllegalArgumentException){
        Log.e("getAndroidColor", "Illegal color $color")
        Color.BLACK
    }
}
