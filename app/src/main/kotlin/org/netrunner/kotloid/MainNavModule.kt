package org.netrunner.kotloid

import android.content.Context
import android.content.Intent
import dagger.Module
import dagger.Provides
import dagger.Subcomponent
import org.netrunner.ControllerScope
import org.netrunner.data.IFactionDataManager
import org.netrunner.data.ITournamentDataManager
import org.netrunner.kotloid.factions.Factions
import org.netrunner.kotloid.factions.Identities
import org.netrunner.kotloid.tournaments.Tournaments
import org.netrunner.routing.Navigation
import org.netrunner.routing.SimpleRouter

@Module
class MainNavModule(val activity: MainNav){
    @Provides
    @ControllerScope
    fun mainMenuRouter(factionManager: IFactionDataManager, dataManager: ITournamentDataManager): SimpleRouter {
        val router = SimpleRouter()
        //Drawer
        router.routes[R.id.nav_current_tournament] =  fun(): Navigation {
            if (factionManager.isValid()) {
                val builder = fun(route: Navigation.ActivityIntent, ctx: Context): Intent {
                    val i = Intent(ctx, route.clazz)
                    i.putExtra(RunTournamentActivity.TOURNAMENT_ID_ARGUMENT, route.id)
                    return i
                }
                return Navigation.ActivityIntent(RunTournamentActivity::class.java, dataManager.currentTournament().id, builder)
            }
            return router[R.id.nav_manage_factions]
        }
        router[R.id.nav_manage_factions] = fun(): Navigation {
            return Navigation.Routable(Factions(), R.string.fragment_faction_list)
        }
        router[R.id.nav_manage_identities] = fun(): Navigation {
            return Navigation.Routable(Identities(), R.string.fragment_faction_list)
        }
        router[R.id.nav_tournament_history] = fun(): Navigation {
            if(factionManager.isValid()) return Navigation.Routable(Tournaments(), R.string.nav_title_tournaments)
            else return router[R.id.nav_manage_factions]
        }
        return router
    }
    @Provides
    @ControllerScope
    fun activity(): MainNav = activity
    @Provides
    @ControllerScope
    fun context(): Context = activity
}


@ControllerScope
@Subcomponent(modules = arrayOf(
        MainNavModule::class
))
interface MainNavComponent{
    fun inject(mainNav: MainNav)
    fun inject(fragment: Factions)
    fun inject(fragment: Identities)
    fun inject(fragment: Tournaments)
}