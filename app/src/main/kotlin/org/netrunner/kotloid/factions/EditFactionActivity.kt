package org.netrunner.kotloid.factions

import android.app.AlertDialog
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.flask.colorpicker.ColorPickerView
import com.flask.colorpicker.builder.ColorPickerDialogBuilder
import dagger.Subcomponent
import kotlinx.android.synthetic.main.activity_add_faction.*
import kotlinx.android.synthetic.main.content_edit_faction.*
import org.netrunner.App
import org.netrunner.ControllerScope
import org.netrunner.kotloid.*
import org.netrunner.kotloid.databinding.ActivityAddFactionBinding
import org.netrunner.kotloid.players.EditPlayerActivity
import org.netrunner.rx.DeleteFaction
import org.netrunner.rx.RxBus
import org.netrunner.rx.UpdateFaction
import org.netrunner.tournament.FactionId
import javax.inject.Inject

@ControllerScope
@Subcomponent
interface EditFactionComponent{
    fun inject(act: EditFactionActivity)
}

class EditFactionActivity : DataActivity<EditFactionVm>() {
    var shouldSave = true
    lateinit override var model: EditFactionVm
    lateinit var originalId: FactionId
    lateinit var component: EditFactionComponent
    @Inject
    lateinit var bus: RxBus

    companion object{
        const val Tag = "edit_faction"
        const val IdTag = "orig_id"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        component = App.appComponent.newEditFactionComponent()
        component.inject(this)
        MainNav.state.menu = R.id.nav_manage_factions

        setupActivity(savedInstanceState, Tag)
        originalId = model.id
        savedInstanceState?.let { //If we saved it (on rotate, etc) load that value instead.
            originalId = it.getString(IdTag)
        }
        val b = DataBindingUtil.setContentView<ActivityAddFactionBinding>(this, R.layout.activity_add_faction)
        b.faction = model
        openColorPickerButton.setOnClickListener {
            showColorPicker()
        }
        colorButton()
        initSpinner()
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    private fun initSpinner() {
        val corp = "corp"
        val runner = "runner"
        val adapter = ArrayAdapter.createFromResource(this, R.array.sides, android.R.layout.simple_spinner_dropdown_item)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        sideSpinner.adapter = adapter
        when (model.sideCode) {
            corp -> sideSpinner.setSelection(0)
            runner -> sideSpinner.setSelection(1)
            else -> {
                sideSpinner.setSelection(0)
                model.sideCode = "corp"
            }
        }
        sideSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {} //Do nothing.
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                when (position) {
                    0 -> model.sideCode = corp
                    1 -> model.sideCode = runner
                    else -> model.sideCode = "invalid"
                }
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        when(id){
            R.id.delete_model-> {
                confirmDelete()
                return true
            }
            R.id.cancel_action -> {
                shouldSave = false
                finish()
                return true
            }
            R.id.save_model -> {
                finish()
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    private fun confirmDelete() {
        AlertDialog.Builder(this)
                .setTitle(R.string.confirm_delete_faction)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(android.R.string.yes, { _, _ ->
                    shouldSave = false
                    bus.send(DeleteFaction(model.toFaction()))
                    finish()
                })
                .setNegativeButton(android.R.string.no, null).show()
    }

    override fun onPause() {
        super.onPause()
        save()
    }

    fun save(){
        if(shouldSave){
            bus.send(UpdateFaction(originalId, model.toFaction()))
        }
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState?.putParcelable(EditPlayerActivity.Tag, model)
        outState?.putString(IdTag, originalId)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.basic_edit_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    fun colorButton(){
        openColorPickerButton.setBackgroundColor(getAndroidColor(model.color))
    }

    fun showColorPicker(){
        ColorPickerDialogBuilder
            .with(this)
                .setTitle(R.string.title_faction_color)
                .initialColor(getAndroidColor(model.color))
                .wheelType(ColorPickerView.WHEEL_TYPE.FLOWER)
                .density(12)
                .setPositiveButton(android.R.string.ok) { _, color, _ ->
                    model.color = getHexColor(color)
                    colorButton()
                }
                .setNegativeButton(android.R.string.cancel, null)
                .build().show()
    }
}
