package org.netrunner.kotloid.factions

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.*
import android.widget.ArrayAdapter
import android.widget.EditText
import android.widget.Toast
import kotlinx.android.synthetic.main.fragment_faction_list.*
import kotlinx.android.synthetic.main.fragment_faction_list.view.*
import org.netrunner.data.IFactionDataManager
import org.netrunner.kotloid.BaseKotloidFragment
import org.netrunner.kotloid.MainNav.Companion.component
import org.netrunner.kotloid.R
import org.netrunner.kotloid.databinding.FactionListItemBinding
import org.netrunner.rx.*
import org.netrunner.tournament.Faction
import javax.inject.Inject

class Factions : BaseKotloidFragment() {
    var factions = listOf<EditFactionVm>()
    @Inject
    override lateinit var bus: RxBus
    @Inject
    lateinit var factionManager: IFactionDataManager


    override fun onCreate(savedInstanceState: Bundle?) {
        component().inject(this)
        super.onCreate(savedInstanceState)
        factions = factionManager.factions().map(::EditFactionVm)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val v = inflater.inflate(R.layout.fragment_faction_list, container, false)
        v.runnerCorpFactionList.adapter = FactionAdapter(v.context, factions)
        v.addFaction.setOnClickListener {
            add()
        }
        v.importFromNrdb.setOnClickListener {
            importFromNetrunnerDb(v, bus)
        }
        return v
    }

    override fun onStart() {
        super.onStart()
        if (!factionManager.isValid()) {
            Toast.makeText(view?.context, R.string.please_setup_factions, Toast.LENGTH_LONG).show()
        }
    }

    override fun handleEvent(e: Any) {
        when (e) {
            is FactionsUpdated -> {
                refreshList()
                Toast.makeText(view?.context, R.string.factions_updated, Toast.LENGTH_SHORT).show()
            }
            is IdentitiesUpdated -> {
                refreshList()
                Toast.makeText(view?.context, R.string.identities_updated, Toast.LENGTH_SHORT).show()
            }
            is FactionUpdated, is FactionDeleted -> {
                refreshList()
            }
            is FactionCreated -> {
                val i = Intent(activity, EditFactionActivity::class.java)
                i.putExtra(EditFactionActivity.Tag, EditFactionVm(e.f))
                activity.startActivity(i)
            }
        }
    }

    fun refreshList() {
        factions = factionManager.factions().map(::EditFactionVm)
        val adapter = this.runnerCorpFactionList.adapter as FactionAdapter
        adapter.notifyDataSetChanged()
    }

    fun add() {
        val txt = EditText(activity)
        txt.setHint(R.string.faction_id_hint)
        val dialog = AlertDialog.Builder(activity)
                .setTitle(R.string.add_faction)
                .setMessage(R.string.enter_faction_id)
                .setView(txt)
                //Using a trick to keep the dialog up on validation failures.
                .setPositiveButton(android.R.string.ok, { _, _ -> })
                .setNegativeButton(android.R.string.cancel, null)
                .create()
        dialog.show()
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener({
            val id = txt.text.toString()
            if (id.isNullOrBlank() || factions.any { it.id == id }) {
                txt.error = resources.getString(R.string.valid_faction_id)
            } else {
                bus.send(CreateFaction(Faction(id = id)))
                dialog.dismiss()
            }
        })
    }
}

class FactionAdapter(ctx: Context, val factions: List<EditFactionVm>) :
        ArrayAdapter<EditFactionVm>(ctx, R.layout.faction_list_item, factions) {
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val p = getItem(position)
        val (cv, binding) = getSafeBindings(convertView, parent)
        binding.faction = p
        cv.tag = binding
        cv.setOnClickListener {
            val i = Intent(context, EditFactionActivity::class.java)
            i.putExtra(EditFactionActivity.Tag, p)
            context.startActivity(i)
        }
        return cv
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val p = getItem(position)
        val (cv, binding) = getSafeBindings(convertView, parent)
        binding.faction = p
        cv.tag = binding
        return cv
    }

    fun getSafeBindings(convertView: View?, parent: ViewGroup?): Pair<View, FactionListItemBinding> {
        if (convertView == null) {
            val inflater = LayoutInflater.from(context)
            val b: FactionListItemBinding = DataBindingUtil.inflate(inflater, R.layout.faction_list_item, parent, false)
            return Pair(b.root, b)
        }
        return Pair(convertView, convertView.tag as FactionListItemBinding)
    }
}