package org.netrunner.kotloid.factions

import android.app.AlertDialog
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.os.Parcel
import android.os.Parcelable
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import dagger.Subcomponent
import kotlinx.android.synthetic.main.activity_add_faction.*
import kotlinx.android.synthetic.main.content_edit_identity.*
import org.netrunner.App
import org.netrunner.ControllerScope
import org.netrunner.data.IFactionDataManager
import org.netrunner.kotloid.DataActivity
import org.netrunner.kotloid.MainNav
import org.netrunner.kotloid.R
import org.netrunner.kotloid.databinding.ActivityEditIdentityBinding
import org.netrunner.kotloid.setupActivity
import org.netrunner.rx.DeleteIdentity
import org.netrunner.rx.RxBus
import org.netrunner.rx.UpdateIdentity
import org.netrunner.tournament.FactionId
import org.netrunner.tournament.Identity
import org.netrunner.tournament.IdentityId
import javax.inject.Inject

data class EditIdentityVm(val code: IdentityId, var name: String, var factionId: FactionId) : Parcelable{
    constructor(i: Identity): this(i.code, i.name, i.factionId)
    fun toIdentity(): Identity = Identity(code, name, factionId)
    companion object {
        @Suppress("unused")
        @JvmField val CREATOR: Parcelable.Creator<EditIdentityVm> = object : Parcelable.Creator<EditIdentityVm> {
            override fun createFromParcel(source: Parcel): EditIdentityVm = EditIdentityVm(source)
            override fun newArray(size: Int): Array<EditIdentityVm?> = arrayOfNulls(size)
        }
    }

    constructor(source: Parcel) : this(source.readString(), source.readString(), source.readString())

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel?, flags: Int) {
        dest?.writeString(code)
        dest?.writeString(name)
        dest?.writeString(factionId)
    }
}

@ControllerScope
@Subcomponent
interface EditIdentityComponent{
    fun inject(act: EditIdentityActivity)
}

class EditIdentityActivity: DataActivity<EditIdentityVm>(){
    var shouldSave = true
    var factions = listOf<EditFactionVm>()
    override lateinit var model: EditIdentityVm
    lateinit var component: EditIdentityComponent
    @Inject
    lateinit var bus: RxBus
    @Inject
    lateinit var factionManager: IFactionDataManager

    companion object{
        const val Tag = "edit_identity"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        component = App.appComponent.newEditIdentityComponent()
        component.inject(this)
        factions = factionManager.factions().map(::EditFactionVm)
        MainNav.state.menu = R.id.nav_manage_identities
        setupActivity(savedInstanceState, Tag)
        val b = DataBindingUtil.setContentView<ActivityEditIdentityBinding>(this, R.layout.activity_edit_identity)
        b.identity = model
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        factionSpinner.adapter =  FactionAdapter(this, factions)
        factionSpinner.setSelection(factions.indexOfFirst { it.id == model.factionId })
        factionSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {}
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
               model.factionId = factions[position].id
            }
        }
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        when(id){
            R.id.delete_model-> {
                confirmDelete()
                return true
            }
            R.id.cancel_action -> {
                shouldSave = false
                finish()
                return true
            }
            R.id.save_model -> {
                finish()
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onPause() {
        super.onPause()
        if(shouldSave) {
            bus.send(UpdateIdentity(model.toIdentity()))
        }
    }

    private fun confirmDelete() {
        AlertDialog.Builder(this)
                .setTitle(R.string.confirm_delete_identity)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(android.R.string.yes, { _, _ ->
                    shouldSave = false
                    bus.send(DeleteIdentity(model.toIdentity()))
                    finish()
                })
                .setNegativeButton(android.R.string.no, null).show()
    }
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.basic_edit_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }
}