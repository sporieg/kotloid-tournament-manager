package org.netrunner.kotloid.factions

import android.os.Parcel
import android.os.Parcelable
import org.netrunner.kotloid.FactionVm
import org.netrunner.tournament.Faction
import org.netrunner.tournament.FactionId

data class EditFactionVm(val id: FactionId = "invalid",
                         var name: String = "Missing",
                         var sideCode: String = "invalid",
                         var color: String = "#ff3300",
                         var isMini: Boolean = false) : Parcelable {
    constructor(source: Parcel) : this(source.readString(), source.readString(), source.readString(), source.readString(), 1.equals(source.readInt()))
    constructor(f: Faction): this(f.id, f.name, f.sideCode, f.color, f.isMini)
    fun toFaction(): Faction {
        return Faction(id = id, color = color, isMini = isMini, name = name, sideCode = sideCode)
    }
    fun toFactionVm(): FactionVm {
        return FactionVm(name, color)
    }

    companion object {
        @Suppress("unused")
        @JvmField val CREATOR: Parcelable.Creator<EditFactionVm> = object : Parcelable.Creator<EditFactionVm> {
            override fun createFromParcel(source: Parcel): EditFactionVm = EditFactionVm(source)
            override fun newArray(size: Int): Array<EditFactionVm?> = arrayOfNulls(size)
        }
    }
    override fun describeContents() = 0
    override fun writeToParcel(dest: Parcel?, flags: Int) {
        dest?.writeString(id)
        dest?.writeString(name)
        dest?.writeString(sideCode)
        dest?.writeString(color)
        dest?.writeInt((if (isMini) 1 else 0))
    }
}