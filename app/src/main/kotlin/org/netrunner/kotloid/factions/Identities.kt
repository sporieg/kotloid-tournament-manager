package org.netrunner.kotloid.factions

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.*
import android.widget.BaseExpandableListAdapter
import android.widget.EditText
import android.widget.Toast
import kotlinx.android.synthetic.main.fragment_identities.view.*
import org.netrunner.data.IFactionDataManager
import org.netrunner.kotloid.BaseKotloidFragment
import org.netrunner.kotloid.FactionVm
import org.netrunner.kotloid.MainNav.Companion.component
import org.netrunner.kotloid.R
import org.netrunner.kotloid.databinding.IdentityListGroupBinding
import org.netrunner.kotloid.databinding.IdentityListItemBinding
import org.netrunner.kotloid.getSafeViewBindings
import org.netrunner.kotloid.players.toIdentityVm
import org.netrunner.rx.*
import org.netrunner.tournament.Faction
import org.netrunner.tournament.Identity
import javax.inject.Inject

typealias IdentitiesVM = Map<Faction, List<Identity>>

class Identities: BaseKotloidFragment(){
    var identities: IdentitiesVM = mapOf()

    @Inject
    override lateinit var bus: RxBus
    @Inject
    lateinit var factionManager: IFactionDataManager

    override fun onCreate(savedInstanceState: Bundle?) {
        component().inject(this)
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        populateData()
        val v = inflater.inflate(R.layout.fragment_identities, container, false)
        v.importFromNrdb.setOnClickListener {
            importFromNetrunnerDb(v, bus)
        }
        v.addIdentity.setOnClickListener {
            add()
        }
        return attachAdapters(v)
    }

    private fun populateData(){
        val factions = factionManager.factions()
        fun factionBy(key: String) = factions.firstOrNull({it.id == key}) ?: Faction()
        val ids = factionManager.identities()
        val mapped: Map<Faction, List<Identity>> = ids.groupBy { it.factionId }.mapKeys { factionBy(it.key) }
        identities = mapped
    }

    private fun attachAdapters(v: View): View{
        val adapter = IdentityAdapter(v.context, identities)
        v.expandIdentityList.setAdapter(adapter)
        for(i in 0 until adapter.groupCount){
            v.expandIdentityList.expandGroup(i)
        }
        return v
    }

    fun add() {
        val txt = EditText(activity)
        txt.setHint(R.string.identity_id_hint)
        val dialog = AlertDialog.Builder(activity)
                .setTitle(R.string.add_identity)
                .setMessage(R.string.enter_identity_id)
                .setView(txt)
                //Using a trick to keep the dialog up on validation failures.
                .setPositiveButton(android.R.string.ok, { _, _ -> })
                .setNegativeButton(android.R.string.cancel, null)
                .create()
        dialog.show()
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener({
            val id = txt.text.toString()
            if(id.isNullOrBlank() || identities.any { it.value.any { it.code == id }}){
                txt.error = resources.getString(R.string.valid_identity_id)
            } else {
                bus.send(CreateIdentity(Identity(code = id)))
                dialog.dismiss()
            }
        })
    }

    override fun handleEvent(e: Any) {
        when(e){
            is FactionsUpdated -> {
                identitiesUpdated(R.string.factions_updated)
            }
            is IdentitiesUpdated -> {
                identitiesUpdated(R.string.identities_updated)
            }
            is IdentityCreated -> {
                val i = Intent(activity, EditIdentityActivity::class.java)
                i.putExtra(EditIdentityActivity.Tag, EditIdentityVm(e.i))
                activity.startActivity(i)
            }
        }
    }

    fun identitiesUpdated(notification: Int){
        populateData()
        view?.let {
            attachAdapters(it)
            Toast.makeText(it.context, notification, Toast.LENGTH_SHORT).show()
        }
    }

}

class IdentityAdapter(val ctx: Context, val gameIds: IdentitiesVM): BaseExpandableListAdapter(){
    val headers = gameIds.keys.sortedWith(compareBy({it.isMini}, { it.sideCode }, {it.id }))
    override fun hasStableIds(): Boolean = true
    fun faction(groupPosition: Int): Faction = headers.getOrNull(groupPosition) ?: Faction()
    override fun getGroupCount(): Int = headers.count()
    override fun getGroup(groupPosition: Int): Any = faction(groupPosition)
    override fun getGroupId(groupPosition: Int): Long = groupPosition.toLong()
    override fun getChildId(groupPosition: Int, childPosition: Int): Long = groupPosition * headers.count() + childPosition.toLong()
    fun identitiesFor(f: Faction) = gameIds[f] ?: listOf()
    override fun getChildrenCount(groupPosition: Int): Int = identitiesFor(faction(groupPosition)).count()
    fun identity(groupPosition: Int, childPosition: Int) = identitiesFor(faction(groupPosition)).getOrNull(childPosition) ?: Identity()
    override fun getChild(groupPosition: Int, childPosition: Int): Any = identity(groupPosition, childPosition)
    override fun isChildSelectable(groupPosition: Int, childPosition: Int): Boolean = true

    fun getSafeGroupBinding(convertView: View?,  parent: ViewGroup?) =
            getSafeViewBindings<IdentityListGroupBinding>(ctx, R.layout.identity_list_group, convertView, parent, { it -> it.tag !is IdentityListGroupBinding})

    override fun getGroupView(groupPosition: Int, isExpanded: Boolean, convertView: View?, parent: ViewGroup?): View {
        val faction = faction(groupPosition)
        val (v, b) = getSafeGroupBinding(convertView, parent)
        b.faction = FactionVm(faction.name, faction.color)
        v.tag = b
        return v
    }

    fun getSaveChildBinding(convertView: View?, parent: ViewGroup?) =
        getSafeViewBindings<IdentityListItemBinding>(ctx, R.layout.identity_list_item, convertView, parent, { it -> it.tag !is IdentityListItemBinding})

    override fun getChildView(groupPosition: Int, childPosition: Int, isLastChild: Boolean, convertView: View?, parent: ViewGroup?): View {
        val faction = faction(groupPosition)
        val id = identity(groupPosition, childPosition)
        val (v, b) =  getSaveChildBinding(convertView, parent)
        b.id = id.toIdentityVm(faction)
        v.tag = b
        v.setOnClickListener{
            val i = Intent(ctx, EditIdentityActivity::class.java)
            i.putExtra(EditIdentityActivity.Tag, EditIdentityVm(id))
            ctx.startActivity(i)
        }
        return v
    }

}
