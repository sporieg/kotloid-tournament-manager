package org.netrunner.kotloid.factions

import android.app.AlertDialog
import android.view.View
import org.netrunner.kotloid.R
import org.netrunner.nrdb.ImportFromNrdb
import org.netrunner.rx.RxBus


fun importFromNetrunnerDb(v: View, bus: RxBus){
    AlertDialog.Builder(v.context)
            .setTitle("Import Factions")
            .setMessage("Import Factions From NRDB?")
            .setIcon(R.drawable.ic_nrdb_icon)
            .setPositiveButton(android.R.string.yes) { _, _ ->
                bus.send(ImportFromNrdb)
            }
            .setNegativeButton(android.R.string.no, null)
            .show()
}