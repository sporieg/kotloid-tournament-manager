package org.netrunner.kotloid

import android.os.Parcel
import android.os.Parcelable
import org.netrunner.tournament.IdentityId

data class FactionVm(var name: String, var color: String) : Parcelable {
    companion object {
        @Suppress("unused")
        @JvmField val CREATOR: Parcelable.Creator<FactionVm> = object : Parcelable.Creator<FactionVm> {
            override fun createFromParcel(source: Parcel): FactionVm = FactionVm(source)
            override fun newArray(size: Int): Array<FactionVm?> = arrayOfNulls(size)
        }
    }
    constructor(source: Parcel) : this(source.readString(), source.readString())
    override fun describeContents() = 0
    override fun writeToParcel(dest: Parcel?, flags: Int) {
        dest?.writeString(name)
        dest?.writeString(color)
    }
}

data class IdentityVm(var title: String, var color: String, var code: IdentityId) : Parcelable {
    companion object {
        @Suppress("unused")
        @JvmField val CREATOR: Parcelable.Creator<IdentityVm> = object : Parcelable.Creator<IdentityVm> {
            override fun createFromParcel(source: Parcel): IdentityVm = IdentityVm(source)
            override fun newArray(size: Int): Array<IdentityVm?> = arrayOfNulls(size)
        }
    }
    constructor(source: Parcel) : this(source.readString(), source.readString(), source.readString())
    override fun describeContents() = 0
    override fun writeToParcel(dest: Parcel?, flags: Int) {
        dest?.writeString(title)
        dest?.writeString(color)
        dest?.writeString(code)
    }
}