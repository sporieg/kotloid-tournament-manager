package org.netrunner.kotloid.tournaments

import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.display_tournament_history_item.view.*
import kotlinx.android.synthetic.main.fragment_tournaments.view.*
import org.netrunner.data.ITournamentDataManager
import org.netrunner.data.TournamentHeaderInfo
import org.netrunner.kotloid.BaseKotloidFragment
import org.netrunner.kotloid.MainNav.Companion.component
import org.netrunner.kotloid.R
import org.netrunner.kotloid.RunTournamentActivity
import org.netrunner.rx.RxBus
import java.text.DateFormat
import java.util.*
import javax.inject.Inject

class Tournaments : BaseKotloidFragment(){
    @Inject
    override lateinit var bus: RxBus
    @Inject
    lateinit var dataManager: ITournamentDataManager

    val model: List<TournamentHeaderInfo> by lazy {
        dataManager.tournamentHeaders()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        component().inject(this)
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val view = inflater.inflate(R.layout.fragment_tournaments, container, false)
        val df = android.text.format.DateFormat.getDateFormat(activity.applicationContext)
        view.tournamentList.let{
            it.adapter = TournamentsAdapter(model, df, this)
            it.layoutManager = LinearLayoutManager(view.context)
        }
        view.addTournament.setOnClickListener {
            val td = dataManager.newTournament()
            goToTournament(td.id)
        }
        return view
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
    }

    fun goToTournament(id: UUID){
        Log.v("Tournaments", "Go to tournament $id")
        val edit = Intent(activity, RunTournamentActivity::class.java)
        edit.putExtra(RunTournamentActivity.TOURNAMENT_ID_ARGUMENT, id)
        startActivity(edit)
    }
}

class TournamentsAdapter(val headers: List<TournamentHeaderInfo>, val dateFormat: DateFormat, val parent: Tournaments):  RecyclerView.Adapter<TournamentsAdapter.MyViewHolder>(){
    class MyViewHolder(v: View): RecyclerView.ViewHolder(v)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val v = LayoutInflater.from(parent.context)
                    .inflate(R.layout.display_tournament_history_item, parent, false)
        return  MyViewHolder(v)
    }

    override fun getItemCount(): Int = headers.count()

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val item = headers[position]
        holder.itemView.let {
            it.date.text = dateFormat.format(item.date)
            it.description.text = item.location
            it.setOnClickListener {
                parent.goToTournament(item.id)
            }
        }
    }
}