package org.netrunner.kotloid.tournaments

import android.os.Parcel
import android.os.Parcelable
import org.netrunner.data.TournamentHeaderInfo
import java.util.*

data class TournamentHeaderVm(val id: UUID, var date: Date, var location: String, var organizer: String): Parcelable {
    companion object {
        @Suppress("unused")
        @JvmField val CREATOR: Parcelable.Creator<TournamentHeaderVm> = object : Parcelable.Creator<TournamentHeaderVm> {
            override fun createFromParcel(source: Parcel): TournamentHeaderVm = TournamentHeaderVm(source)
            override fun newArray(size: Int): Array<TournamentHeaderVm?> = arrayOfNulls(size)
        }
    }

    constructor(source: Parcel) : this(source.readSerializable() as UUID, source.readSerializable() as Date, source.readString(), source.readString())

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel?, flags: Int) {
        dest?.writeSerializable(id)
        dest?.writeSerializable(date)
        dest?.writeString(location)
        dest?.writeString(organizer)
    }
}

fun TournamentHeaderInfo.toTournamentHeaderVm() = TournamentHeaderVm(
        this.id,
        this.date,
        this.location,
        this.organizer
)