package org.netrunner.kotloid.tournaments

import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import dagger.Subcomponent
import kotlinx.android.synthetic.main.activity_edit_player.*
import org.netrunner.App
import org.netrunner.ControllerScope
import org.netrunner.data.ITournamentDataManager
import org.netrunner.kotloid.DataActivity
import org.netrunner.kotloid.R
import org.netrunner.kotloid.RunTournamentActivity
import org.netrunner.kotloid.databinding.ActivityEditTournamentHeaderBinding
import org.netrunner.kotloid.setupActivity
import org.netrunner.rx.RxBus
import org.netrunner.rx.UpdateHeader
import java.util.*
import javax.inject.Inject

@ControllerScope
@Subcomponent
interface TournamentHeaderComponent{
    fun inject(editPlayer: TournamentHeaderActivity)
}


class TournamentHeaderActivity: DataActivity<TournamentHeaderVm>(){
    @Inject
    lateinit var bus: RxBus
    @Inject
    lateinit var dataManager: ITournamentDataManager
    lateinit var tournamentId: UUID
    lateinit override var model: TournamentHeaderVm
    lateinit var component: TournamentHeaderComponent

    companion object{
        val Tag = "tournament_header"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        component = App.appComponent.newTournamentHeaderComponent()
        component.inject(this)
        setupActivity(savedInstanceState, Tag)
        val b = DataBindingUtil.setContentView<ActivityEditTournamentHeaderBinding>(this, R.layout.activity_edit_tournament_header)
        b.header = model
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun getParentActivityIntent(): Intent {
        val i = super.getParentActivityIntent()
        i.putExtra(RunTournamentActivity.TOURNAMENT_ID_ARGUMENT, model.id)
        return i
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState?.putParcelable(Tag, model)
    }

    override fun onPause() {
        super.onPause()
        bus.send(UpdateHeader(model.id, model.location, model.organizer))
    }
}
