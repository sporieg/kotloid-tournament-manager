package org.netrunner.kotloid.timer

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_round_timer.view.*
import org.netrunner.kotloid.BaseKotloidFragment
import org.netrunner.kotloid.R
import org.netrunner.kotloid.RunTournamentActivity.Companion.component
import org.netrunner.kotloid.databinding.FragmentRoundTimerBinding
import org.netrunner.rx.RxBus
import javax.inject.Inject


class RoundTimerHandler{
    companion object {
        val STANDARD_ROUND_MINUTES = 65
        val DRAFT_ROUND_MINUTES = 50
        val HALF_DRAFT_ROUND_MINUTES = 30
        val DOUBLE_ELIM_ROUND_MINUTES = 40
        val ELIM_FINALS_ROUND_MINUTES = 60
    }
    fun startSwissRound(v: View) = startTimer(v, STANDARD_ROUND_MINUTES)
    fun startDoubleEliminationRound(v: View) = startTimer(v, DOUBLE_ELIM_ROUND_MINUTES)
    fun startEliminationFinals(v: View) = startTimer(v, ELIM_FINALS_ROUND_MINUTES)
    fun startDraftRound(v: View) = startTimer(v, DRAFT_ROUND_MINUTES)
    fun startHalfDraftRound(v: View) = startTimer(v, HALF_DRAFT_ROUND_MINUTES)


    fun startTimer(v: View, minutes: Int){
        val c = AlarmTimeBuilder(minutes)
        val msg = v.context.resources.getString(R.string.round_timer_alarm)
        val clockIntent = AlarmIntentBuilder(msg, c.hour, c.minutes)
        v.context.startActivity(clockIntent.clockIntent)
    }
}


class RoundTimer: BaseKotloidFragment(){

    @Inject
    override lateinit var bus: RxBus

    init{
        setHasOptionsMenu(true)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        component().inject(this)
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val b = FragmentRoundTimerBinding.inflate(inflater, container, false)
        b.handler = RoundTimerHandler()
        val v = b.root
        val res = v.context.resources
        v.swissRound.text = res.getString(R.string.swiss_round_minutes).format(RoundTimerHandler.STANDARD_ROUND_MINUTES)
        v.doubleElimination.text = res.getString(R.string.double_elimination_minutes).format(RoundTimerHandler.DOUBLE_ELIM_ROUND_MINUTES)
        v.finalElimination.text = res.getString(R.string.finals_minutes).format(RoundTimerHandler.ELIM_FINALS_ROUND_MINUTES)
        v.draftRound.text = res.getString(R.string.draft_round_minutes).format(RoundTimerHandler.DRAFT_ROUND_MINUTES)
        v.halfDraftRound.text = res.getString(R.string.half_draft_round_minutes).format(RoundTimerHandler.HALF_DRAFT_ROUND_MINUTES)
        return v
    }

}