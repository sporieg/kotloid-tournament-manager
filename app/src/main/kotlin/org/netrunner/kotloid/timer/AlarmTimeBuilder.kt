package org.netrunner.kotloid.timer

import java.util.*

typealias Minutes = Int
class AlarmTimeBuilder(roundDuration: Minutes, calendar: Calendar = Calendar.getInstance(), now: Date = Date()){
    init{
        calendar.time = now
        calendar.set(Calendar.SECOND, 0 )
        calendar.set(Calendar.MILLISECOND, 0 )
        calendar.add(Calendar.MINUTE, roundDuration)
    }
    val minutes = calendar.get(Calendar.MINUTE)
    val hour = calendar.get(Calendar.HOUR_OF_DAY)
    val millis = calendar.timeInMillis
}