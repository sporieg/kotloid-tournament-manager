package org.netrunner.kotloid.timer

import android.content.Intent
import android.provider.AlarmClock

class AlarmIntentBuilder(msg: String = "No Text Set", hourOfDay: Int = 0, minutes: Int = 0) {
    val clockIntent = Intent(AlarmClock.ACTION_SET_ALARM)
/*
    fun pendingIntent(context: Context) = PendingIntent.getBroadcast(context, 9814039, clockIntent, PendingIntent.FLAG_CANCEL_CURRENT)!!
*/
    init{
        clockIntent.putExtra(AlarmClock.EXTRA_MESSAGE, msg)
        clockIntent.putExtra(AlarmClock.EXTRA_SKIP_UI, true)
        clockIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        clockIntent.putExtra(AlarmClock.EXTRA_MINUTES, minutes)
        clockIntent.putExtra(AlarmClock.EXTRA_HOUR, hourOfDay)
    }
}