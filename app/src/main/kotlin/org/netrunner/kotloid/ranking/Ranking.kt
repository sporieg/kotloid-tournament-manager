package org.netrunner.kotloid.ranking

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.fragment_player_ranking.view.*
import kotlinx.android.synthetic.main.ranked_player_list_item.view.*
import org.netrunner.data.TournamentData
import org.netrunner.kotloid.BaseKotloidFragment
import org.netrunner.kotloid.R
import org.netrunner.kotloid.RunTournamentActivity.Companion.component
import org.netrunner.kotloid.databinding.RankedPlayerListItemBinding
import org.netrunner.kotloid.getSafeViewBindings
import org.netrunner.rx.DefaultSub
import org.netrunner.rx.RxBus
import org.netrunner.tournament.swiss.SwissRanker
import javax.inject.Inject

class Ranking : BaseKotloidFragment() {
    lateinit var fmt: PlayerFormatter
    @Inject
    override lateinit var bus: RxBus
    @Inject
    lateinit var td: TournamentData
    @Inject
    lateinit var actBus: PublishSubject<TournamentData>
    var actSub: Disposable = DefaultSub
    init {
        setHasOptionsMenu(true)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        component().inject(this)
        actSub = actBus.subscribe{update(it)}
        super.onCreate(savedInstanceState)
        fmt = PlayerFormatter(resources)
    }

    override fun onDestroy() {
        super.onDestroy()
        if(!actSub.isDisposed) actSub.dispose()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return attachAdapters(SetupViewBindings(inflater, container))
    }

    private fun SetupViewBindings(inflater: LayoutInflater, container: ViewGroup?): View {
        return inflater.inflate(R.layout.fragment_player_ranking, container, false)
    }

    private fun attachAdapters(view: View): View {
        val ranker = SwissRanker(td.tournament)
        view.playerRankingList.adapter = PlayerRankingAdapter(view.context, ranker.rankedPlayers()
                .mapIndexed { index, player -> RankedPlayerVm(index + 1, player, td) }, fmt, this)
        return view
    }

    private fun update(tournamentData: TournamentData){
        td = tournamentData
        view?.let { attachAdapters(it) }
    }
}

class PlayerRankingAdapter(ctx: Context, val players: List<RankedPlayerVm>, val fmt: PlayerFormatter, val parent: Ranking) :
        ArrayAdapter<RankedPlayerVm>(ctx, R.layout.ranked_player_list_item, players) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val p = getItem(position)
        val (cv, b) = getSafeBindings(convertView, parent)
        b.setPlayer(p)
        b.setFmt(fmt)
        cv.tag = b
        cv.infoButton.setOnClickListener {
            openDetailsDialog(p)
        }
        return cv
    }

    fun getSafeBindings(cv: View?, p: ViewGroup?): Pair<View, RankedPlayerListItemBinding> {
        fun check(v: View) = v.tag !is RankedPlayerListItemBinding
        val layout = R.layout.ranked_player_list_item
        return getSafeViewBindings(context, layout, cv, p, ::check)
    }

    fun openDetailsDialog(player: RankedPlayerVm) {
        val i = Intent(parent.activity, PlayerRankDetailsActivity::class.java)
        i.putExtra(PlayerRankDetailsActivity.Tag, player)
        parent.activity.startActivity(i)
    }
}
