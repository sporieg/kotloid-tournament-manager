package org.netrunner.kotloid.ranking

import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.OrientationHelper
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import dagger.Subcomponent
import kotlinx.android.synthetic.main.activity_player_rank_details.*
import kotlinx.android.synthetic.main.display_match_history.view.*
import org.netrunner.App
import org.netrunner.ControllerScope
import org.netrunner.data.ITournamentDataManager
import org.netrunner.kotloid.DataActivity
import org.netrunner.kotloid.R
import org.netrunner.kotloid.RunTournamentActivity
import org.netrunner.kotloid.databinding.ActivityPlayerRankDetailsBinding
import org.netrunner.kotloid.setupActivity
import org.netrunner.tournament.swiss.SwissBye
import org.netrunner.tournament.swiss.SwissPlayerPairing
import javax.inject.Inject


@ControllerScope
@Subcomponent
interface PlayerRankDetailsComponent{
    fun inject(editPlayer: PlayerRankDetailsActivity)
}

data class PlayerHistory(val round: Int, val opponent: String, val myPoints: Int)

class PlayerRankDetailsActivity : DataActivity<RankedPlayerVm>() {
    @Inject
    lateinit var data: ITournamentDataManager
    lateinit var component: PlayerRankDetailsComponent
    override lateinit var model: RankedPlayerVm
    val tournament = lazy{
        data.tournament(model.tournamentId).tournament
    }
    companion object{
        val Tag = "player_rank_details"
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        component = App.appComponent.newPlayerRankDetailsComponent()
        component.inject(this)

        setupActivity(savedInstanceState, Tag)
        val b = DataBindingUtil.setContentView<ActivityPlayerRankDetailsBinding>(this, R.layout.activity_player_rank_details)
        b.player = model
        b.fmt = PlayerFormatter(resources)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        setUpMatchHistoryList()
    }

    override fun getParentActivityIntent(): Intent {
        val i  =super.getParentActivityIntent()
        i.putExtra(RunTournamentActivity.TOURNAMENT_ID_ARGUMENT, model.tournamentId)
        i.putExtra(RunTournamentActivity.START_PAGE_SECTION_ID, R.string.tournament_menu_ranking)
        return i
    }

    fun setUpMatchHistoryList(){
        val t = tournament.value
        val hist = t.roundHistoryFor(model.playerId).map {
            when(it){
                is SwissPlayerPairing -> PlayerHistory(it.round, t.player(it.opponentOf(model.playerId))?.nickName ?: "", it.pointsFor(model.playerId))
                is SwissBye -> PlayerHistory(it.round, "", it.pointsFor(model.playerId))
                else -> PlayerHistory(0, "", 0)
            }
        }
        matchHistory.adapter = MatchHistoryAdapter(model, hist, PlayerFormatter(resources))
        matchHistory.layoutManager = LinearLayoutManager(this, OrientationHelper.VERTICAL, false)
        matchHistory.itemAnimator = DefaultItemAnimator()
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState?.putParcelable(Tag, model)
    }
}

class MatchHistoryAdapter(val player: RankedPlayerVm, val hist: List<PlayerHistory>, val fmt: PlayerFormatter):
        RecyclerView.Adapter<MatchHistoryAdapter.ViewHolder>(){
    class ViewHolder(v: View): RecyclerView.ViewHolder(v)
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MatchHistoryAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.display_match_history, parent, false)
        return ViewHolder(v)
    }
    override fun onBindViewHolder(holder: MatchHistoryAdapter.ViewHolder, position: Int) {
        val item = hist[position]
        holder.itemView.roundNum.text = item.round.toString()
        holder.itemView.points.text = fmt.formatPoints(item.myPoints)
        holder.itemView.opponent.text = item.opponent
    }

    override fun getItemCount(): Int = hist.count()


}
