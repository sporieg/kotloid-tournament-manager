package org.netrunner.kotloid.ranking

import android.content.res.Resources
import android.os.Parcel
import android.os.Parcelable
import org.netrunner.data.TournamentData
import org.netrunner.kotloid.IdentityVm
import org.netrunner.kotloid.R
import org.netrunner.kotloid.players.toIdentityVm
import org.netrunner.tournament.Player
import org.netrunner.tournament.PlayerId
import org.netrunner.tournament.swiss.extendedStrengthOfSchedule
import org.netrunner.tournament.swiss.points
import org.netrunner.tournament.swiss.strengthOfSchedule
import java.util.*

data class RankedPlayerVm(val playerRank: Int = 1,
                          val playerId: PlayerId = 0,
                          val nickName: String = "",
                          val points: Int = 0,
                          val strengthOfSchedule: Double = 0.0,
                          val extendedStrengthOfSchedule: Double = 0.0,
                          val tournamentId: UUID,
                          val corpId: IdentityVm,
                          val runnerId: IdentityVm) : Parcelable{
    constructor(r: Int, p: Player, td: TournamentData):
    this(r, p.id, p.nickName,
            p.points(td.tournament),
            p.strengthOfSchedule(td.tournament),
            p.extendedStrengthOfSchedule(td.tournament),
            td.id,
            p.corp.toIdentityVm(),
            p.runner.toIdentityVm())

    companion object {
        @Suppress("unused")
        @JvmField val CREATOR: Parcelable.Creator<RankedPlayerVm> = object : Parcelable.Creator<RankedPlayerVm> {
            override fun createFromParcel(source: Parcel): RankedPlayerVm = RankedPlayerVm(source)
            override fun newArray(size: Int): Array<RankedPlayerVm?> = arrayOfNulls(size)
        }
    }

    constructor(source: Parcel) : this(source.readInt(), source.readInt(), source.readString(), source.readInt(), source.readDouble(), source.readDouble(), source.readSerializable() as UUID, source.readParcelable<IdentityVm>(IdentityVm::class.java.classLoader), source.readParcelable<IdentityVm>(IdentityVm::class.java.classLoader))

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel?, flags: Int) {
        dest?.writeInt(playerRank)
        dest?.writeInt(playerId)
        dest?.writeString(nickName)
        dest?.writeInt(points)
        dest?.writeDouble(strengthOfSchedule)
        dest?.writeDouble(extendedStrengthOfSchedule)
        dest?.writeSerializable(tournamentId)
        dest?.writeParcelable(corpId, 0)
        dest?.writeParcelable(runnerId, 0)
    }
}

class PlayerFormatter(var rankFormat: String = "%corpIdKey.",
                      var pointsFrmt: String = "%d Points",
                      var sosFormat: String = "SoS: %.4f",
                      var esosFormat: String = "eSos: %.4f"){
    fun formatRank(p: RankedPlayerVm?): String = rankFormat.format(p?.playerRank)
    fun formatPoints(p: RankedPlayerVm?): String = pointsFrmt.format(p?.points)
    fun formatPoints(points: Int): String = pointsFrmt.format(points)
    fun formatSos(p: RankedPlayerVm?): String = sosFormat.format(p?.strengthOfSchedule)
    fun formatEsos(p: RankedPlayerVm?): String = esosFormat.format(p?.extendedStrengthOfSchedule)

    constructor(resources: Resources) : this(rankFormat = resources.getString(R.string.rank_format),
            pointsFrmt = resources.getString(R.string.points_format),
            sosFormat = resources.getString(R.string.sos_format),
            esosFormat = resources.getString(R.string.e_sos_format))
}