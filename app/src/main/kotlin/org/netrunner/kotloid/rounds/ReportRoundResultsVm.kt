package org.netrunner.kotloid.rounds

import android.databinding.ObservableField
import android.databinding.ObservableInt
import android.os.Parcel
import android.os.Parcelable
import org.netrunner.rx.GameReport
import org.netrunner.rx.ReportSwissRound
import org.netrunner.tournament.Player
import org.netrunner.tournament.Points.Loss
import org.netrunner.tournament.Points.Win
import org.netrunner.tournament.swiss.SwissGame
import org.netrunner.tournament.swiss.SwissPlayerPairing
import org.netrunner.tournament.swiss.WinType
import java.util.*

data class GameVm(var runnerPoints: ObservableInt = ObservableInt(0),
                  var corpPoints: ObservableInt = ObservableInt(0),
                  var winType: ObservableField<WinType> = ObservableField( WinType.Points)) : Parcelable {
    constructor(rPoints: Int, cPoints: Int, wType: WinType): this(ObservableInt(rPoints), ObservableInt(cPoints), ObservableField(wType))

    constructor(g: SwissGame) : this(g.runnerPoints, g.corpPoints, g.winType)

    fun runnerWin(t: WinType = WinType.Points) {
        runnerPoints.set(Win.points)
        corpPoints.set(Loss.points)
        winType.set(t)
    }

    fun corpWin(t: WinType = WinType.Points) {
        runnerPoints.set(Loss.points)
        corpPoints.set(Win.points)
        winType.set(t)
    }

    companion object {
        @Suppress("unused")
        @JvmField val CREATOR: Parcelable.Creator<GameVm> = object : Parcelable.Creator<GameVm> {
            override fun createFromParcel(source: Parcel): GameVm = GameVm(source)
            override fun newArray(size: Int): Array<GameVm?> = arrayOfNulls(size)
        }
    }

    constructor(source: Parcel) : this(source.readParcelable<ObservableInt>(ObservableInt::class.java.classLoader), source.readParcelable<ObservableInt>(ObservableInt::class.java.classLoader), source.readSerializable() as ObservableField<WinType>)

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel?, flags: Int) {
        dest?.writeParcelable(runnerPoints, 0)
        dest?.writeParcelable(corpPoints, 0)
        dest?.writeSerializable(winType)
    }
}

data class PlayerReportVM(val id: Int = 0, val nickName: String = "") : Parcelable {
    companion object {
        @Suppress("unused")
        @JvmField val CREATOR: Parcelable.Creator<PlayerReportVM> = object : Parcelable.Creator<PlayerReportVM> {
            override fun createFromParcel(source: Parcel): PlayerReportVM = PlayerReportVM(source)
            override fun newArray(size: Int): Array<PlayerReportVM?> = arrayOfNulls(size)
        }
    }

    constructor(source: Parcel) : this(source.readInt(), source.readString())

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel?, flags: Int) {
        dest?.writeInt(id)
        dest?.writeString(nickName)
    }
}


data class ReportRoundResultsVm(val id: UUID,
                                val roundNum: Int,
                                val p1: PlayerReportVM,
                                val p2: PlayerReportVM,
                                //Player 1 runner game
                                val game1: GameVm,
                                //Player 1 corp game
                                val game2: GameVm) : Parcelable {
    var formattedPoints: ObservableField<String> = ObservableField(pointString)

    val pointString: String
    get() = "${game1.runnerPoints.get() + game2.corpPoints.get()}:${game1.corpPoints.get() + game2.runnerPoints.get()}"

    fun runnerSplit() {
        game1.runnerWin()
        game2.runnerWin()
        formattedPoints.set(pointString)
    }

    fun corpSplit() {
        game1.corpWin()
        game2.corpWin()
        formattedPoints.set(pointString)
    }

    fun playerOneSweep() {
        game1.runnerWin()
        game2.corpWin()
        formattedPoints.set(pointString)
    }

    fun playerTwoSweep() {
        game1.corpWin()
        game2.runnerWin()
        formattedPoints.set(pointString)
    }

    fun intentionalDraw() {
        game1.runnerWin(WinType.IntentionalDraw)
        game2.runnerWin(WinType.IntentionalDraw)
        formattedPoints.set(pointString)
    }

    constructor(id: UUID,
                roundNum: Int,
                pair: SwissPlayerPairing) :
    this(id = id,
            roundNum = roundNum,
            p1 = PlayerReportVM(pair.p1),
            p2 = PlayerReportVM(pair.p2),
            game1 = GameVm(pair.match.runnerGameFor(pair.p1)),
            game2 = GameVm(pair.match.corpGameFor(pair.p1)))

    companion object {
        @Suppress("unused")
        @JvmField val CREATOR: Parcelable.Creator<ReportRoundResultsVm> = object : Parcelable.Creator<ReportRoundResultsVm> {
            override fun createFromParcel(source: Parcel): ReportRoundResultsVm = ReportRoundResultsVm(source)
            override fun newArray(size: Int): Array<ReportRoundResultsVm?> = arrayOfNulls(size)
        }
    }

    constructor(source: Parcel) : this(source.readSerializable() as UUID, source.readInt(), source.readParcelable<PlayerReportVM>(PlayerReportVM::class.java.classLoader), source.readParcelable<PlayerReportVM>(PlayerReportVM::class.java.classLoader), source.readParcelable<GameVm>(GameVm::class.java.classLoader), source.readParcelable<GameVm>(GameVm::class.java.classLoader))

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel?, flags: Int) {
        dest?.writeSerializable(id)
        dest?.writeInt(roundNum)
        dest?.writeParcelable(p1, 0)
        dest?.writeParcelable(p2, 0)
        dest?.writeParcelable(game1, 0)
        dest?.writeParcelable(game2, 0)
    }
}

fun Player.toPlayerReportVm(): PlayerReportVM = PlayerReportVM(id, nickName)
fun SwissGame.toGameVm(): GameVm = GameVm(runnerPoints, corpPoints, winType)
fun ReportRoundResultsVm.toReportSwissRound(): ReportSwissRound {
    val p1 = p1.id
    val p2 = p2.id
    val g1 = game1
    val g2 = game2
    return ReportSwissRound(id,
            roundNum,
            GameReport(p1, p2, g1.runnerPoints.get(), g1.corpPoints.get(), g1.winType.get()),
            GameReport(p2, p1, g2.runnerPoints.get(), g2.corpPoints.get(), g2.winType.get()))
}