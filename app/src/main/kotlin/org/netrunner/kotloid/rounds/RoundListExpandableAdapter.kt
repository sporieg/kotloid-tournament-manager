package org.netrunner.kotloid.rounds

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.res.Resources
import android.view.View
import android.view.ViewGroup
import android.widget.BaseExpandableListAdapter
import android.widget.Toast
import kotlinx.android.synthetic.main.round_list_group.view.*
import org.netrunner.data.TournamentData
import org.netrunner.kotloid.R
import org.netrunner.kotloid.databinding.RoundListGroupBinding
import org.netrunner.kotloid.databinding.RoundListItemByeBinding
import org.netrunner.kotloid.databinding.RoundListItemPairingBinding
import org.netrunner.kotloid.getSafeViewBindings
import org.netrunner.rx.DropLast
import org.netrunner.rx.PairRound
import org.netrunner.rx.RxBus
import org.netrunner.tournament.swiss.SwissBye
import org.netrunner.tournament.Pairing
import org.netrunner.tournament.swiss.SwissPlayerPairing
import org.netrunner.tournament.swiss.SwissRound
import java.util.*

class RoundListExpandableAdapter(val t: TournamentData, val ctx: Context, val rounds: Rounds, val bus: RxBus) : BaseExpandableListAdapter() {
    val roundsDescending = t.tournament.swissSwissRounds.sortedByDescending { it.num }
    override fun isChildSelectable(groupPosition: Int, childPosition: Int): Boolean = true
    override fun hasStableIds(): Boolean = true
    override fun getGroup(groupPosition: Int): Any = roundsDescending[groupPosition]
    //Put byes last in the listing.
    override fun getChild(groupPosition: Int, childPosition: Int): Any = roundsDescending[groupPosition].pairings.sortedBy { it is SwissBye }[childPosition]

    override fun getGroupCount(): Int = roundsDescending.count()
    override fun getChildrenCount(groupPosition: Int): Int = roundsDescending.getOrElse(groupPosition, { _ -> SwissRound(-1, listOf()) }).pairings.count()
    override fun getGroupView(groupPosition: Int, isExpanded: Boolean, convertView: View?, parent: ViewGroup?): View {
        val (cv, binding) = getSafeBindings(convertView, parent)
        val round = getGroup(groupPosition) as SwissRound
        val rvm = RoundViewModel(round, round.num == groupCount, t.id)
        binding.r =rvm
        cv.tag = binding
        cv.pairRound.setOnClickListener {
            AlertDialog.Builder(cv.context)
                    .setTitle(org.netrunner.kotloid.R.string.confirm_pair_round)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setPositiveButton(android.R.string.yes, { _, _ ->
                        bus.send(PairRound(rvm.r, rvm.id))
                        Toast.makeText(cv.context, org.netrunner.kotloid.R.string.round_paired, Toast.LENGTH_SHORT).show()
                    })
                    .setNegativeButton(android.R.string.no, null).show()
        }
        cv.deleteRound.setOnClickListener {
            AlertDialog.Builder(cv.context)
                    .setTitle(org.netrunner.kotloid.R.string.confirm_remove_round)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setPositiveButton(android.R.string.yes, { _, _ ->
                        bus.send(DropLast(rvm.id))
                        Toast.makeText(cv.context, org.netrunner.kotloid.R.string.round_removed, Toast.LENGTH_SHORT).show()
                    })
                    .setNegativeButton(android.R.string.no, null).show()
        }
        val res = ctx.resources
        cv.roundListingHeader.text = res.getString(R.string.fragment_rounds_group_header).format(round.num)
        return cv
    }

    fun getSafeBindings(convertView: View?, parent: ViewGroup?): Pair<View, RoundListGroupBinding> {
        fun check(v: View) = v.tag !is RoundListGroupBinding
        val layout = R.layout.round_list_group
        return getSafeViewBindings(ctx, layout, convertView, parent, ::check)
    }

    override fun getChildView(groupPosition: Int, childPosition: Int, isLastChild: Boolean, convertView: View?, parent: ViewGroup?): View {
        val pairing = getChild(groupPosition, childPosition) as Pairing
        val round = getGroup(groupPosition) as SwissRound
        val res: Resources = ctx.resources
        when (pairing) {
            is SwissBye -> {
                val (v, b) = getSafeByeBinding(convertView, parent)
                b.bye = ByeViewModel(t.tournament.activePlayer(pairing.byePlayer)!!.nickName, pairing.pointsFor(pairing.byePlayer), res)
                v.tag = b
                return v
            }
            is SwissPlayerPairing -> {
                val (v, b) = getSafeChildBindings(convertView, parent)
                val p1 = t.tournament.activePlayer(pairing.p1)!!
                val p2 = t.tournament.activePlayer(pairing.p2)!!
                b.pairing = PairingViewModel(p1.nickName, p2.nickName, pairing.player1Points(), pairing.player2Points(), res)
                v.tag = b
                v.setOnClickListener { _ ->
                    val i = Intent(rounds.activity, ReportRoundResultsActivity::class.java)
                    val vm = ReportRoundResultsVm(t.id, round.num, p1.toPlayerReportVm(), p2.toPlayerReportVm(), pairing.match.runnerGameFor(p1.id).toGameVm(), pairing.match.corpGameFor(p1.id).toGameVm())
                    i.putExtra(ReportRoundResultsActivity.Tag, vm)
                    rounds.startActivity(i)
                }
                return v
            }
            else -> TODO("UNSUPPORTED ROUND TYPE")
        }
    }

    fun getSafeByeBinding(convertView: View?, parent: ViewGroup?): Pair<View, RoundListItemByeBinding> {
        fun check(v: View) = v.tag !is RoundListItemByeBinding
        val layout = R.layout.round_list_item_bye
        return getSafeViewBindings(ctx, layout, convertView, parent, ::check)
    }

    fun getSafeChildBindings(convertView: View?, parent: ViewGroup?): Pair<View, RoundListItemPairingBinding> {
        fun check(v: View) = v.tag !is RoundListItemPairingBinding
        val layout = R.layout.round_list_item_pairing
        return getSafeViewBindings(ctx, layout, convertView, parent, ::check)
    }

    override fun getGroupId(groupPosition: Int): Long = groupPosition.toLong()
    override fun getChildId(groupPosition: Int, childPosition: Int): Long = childPosition.toLong()
}

data class RoundViewModel(val r: SwissRound, val last: Boolean, val id: UUID)
data class ByeViewModel(val nickName: String, val points: Int, val r: Resources) {
    fun formattedPoints(): String = r.getQuantityString(R.plurals.fragment_rounds_list_item_bye_points, points).format(points)
}

data class PairingViewModel(val player1NickName: String, val player2NickName: String, val player1Points: Int, val player2Points: Int, val r: Resources) {
    fun formattedPoints(p: Int): String = r.getQuantityString(R.plurals.fragment_rounds_list_item_bye_points, p).format(p)
}