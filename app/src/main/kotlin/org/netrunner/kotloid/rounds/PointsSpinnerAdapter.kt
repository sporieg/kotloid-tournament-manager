package org.netrunner.kotloid.rounds

import android.content.Context
import android.widget.ArrayAdapter
import org.netrunner.tournament.Points

class PointsSpinnerAdapter(ctx: Context, val points: List<Points>): ArrayAdapter<Points>(ctx, android.R.layout.simple_spinner_item, points)

