package org.netrunner.kotloid.rounds

import android.content.Intent
import android.databinding.DataBindingUtil
import android.databinding.ObservableInt
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Spinner
import dagger.Subcomponent
import kotlinx.android.synthetic.main.activity_edit_player.*
import kotlinx.android.synthetic.main.content_report_round_results.view.*
import org.netrunner.App
import org.netrunner.ControllerScope
import org.netrunner.kotloid.DataActivity
import org.netrunner.kotloid.R
import org.netrunner.kotloid.RunTournamentActivity
import org.netrunner.kotloid.databinding.ActivityReportRoundResultsBinding
import org.netrunner.kotloid.setupActivity
import org.netrunner.rx.RxBus
import org.netrunner.rx.RxUtils.toObservable
import org.netrunner.tournament.Points
import javax.inject.Inject


@ControllerScope
@Subcomponent
interface ReportRoundResultsActivityComponent{
    fun inject(results: ReportRoundResultsActivity)
}


class ReportRoundResultsActivity: DataActivity<ReportRoundResultsVm>(){
    companion object{
        val Tag = "round_report"
    }
    @Inject
    lateinit var bus: RxBus
    override lateinit var model: ReportRoundResultsVm
    var shouldSave = true

    lateinit var component: ReportRoundResultsActivityComponent

    override fun onCreate(savedInstanceState: Bundle?) {
        component = App.appComponent.newPlayerRoundResultsComponent()
        component.inject(this)
        super.onCreate(savedInstanceState)
        setupActivity(savedInstanceState, Tag)
        val b: ActivityReportRoundResultsBinding = DataBindingUtil.setContentView(this, R.layout.activity_report_round_results)
        b.swissRound = model
        val v = b.root
        bindTemplateStrings(attachAdapters(v))
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun getParentActivityIntent(): Intent {
        val i = super.getParentActivityIntent()
        i.putExtra(RunTournamentActivity.TOURNAMENT_ID_ARGUMENT, model.id)
        i.putExtra(RunTournamentActivity.START_PAGE_SECTION_ID, R.string.tournament_menu_rounds)
        return i
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.activity_report_results_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        when(id){
            R.id.save_action -> {
                finish()
                return true
            }
            R.id.cancel_action -> {
                shouldSave = false
                finish()
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onPause() {
        super.onPause()
        if(shouldSave) report(model)
    }

    fun report(results: ReportRoundResultsVm){
        bus.send(results.toReportSwissRound())
    }

    fun attachAdapters(v: View): View {
        bind(model.game1.runnerPoints, v.player1RunnerPoints)
        bind(model.game1.corpPoints, v.player2CorpPoints)
        bind(model.game2.corpPoints, v.player1CorpPoints)
        bind(model.game2.runnerPoints, v.player2RunnerPoints)
        return v
    }

    fun bind(field: ObservableInt, spinner: Spinner){
        val observable = field.toObservable()
        spinner.adapter = PointsSpinnerAdapter(spinner.context, Points.all())
        spinner.setSelection(field.get())
        observable.subscribe {
            spinner.setSelection(it)
        }
    }

    fun bindTemplateStrings(v: View): View{
        val corp = v.resources.getString(R.string.player_corp)
        val runner = v.resources.getString(R.string.player_runner)
        v.player1CorpLabel.text = corp.format(model.p1.nickName)
        v.player1RunnerLabel.text = runner.format(model.p1.nickName)
        v.player2CorpLabel.text = corp.format(model.p2.nickName)
        v.player2RunnerLabel.text = runner.format(model.p2.nickName)
        return v
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState?.putParcelable(Tag, model)
    }
}