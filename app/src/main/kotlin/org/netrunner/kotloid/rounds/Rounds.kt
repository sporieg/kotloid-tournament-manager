package org.netrunner.kotloid.rounds


import android.app.Fragment
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.fragment_rounds.view.*
import org.netrunner.data.TournamentData
import org.netrunner.kotloid.BaseKotloidFragment
import org.netrunner.kotloid.RunTournamentActivity.Companion.component
import org.netrunner.kotloid.databinding.FragmentRoundsBinding
import org.netrunner.rx.DefaultSub
import org.netrunner.rx.RxBus
import javax.inject.Inject


/**
 * A simple [Fragment] subclass.
 */
class Rounds: BaseKotloidFragment() {
    @Inject
    override lateinit var bus: RxBus
    @Inject
    lateinit var td: TournamentData
    var actSub: Disposable = DefaultSub
    @Inject
    lateinit var actBus: PublishSubject<TournamentData>
    init {
        setHasOptionsMenu(true)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        component().inject(this)
        actSub = actBus.subscribe{update(it)}
        super.onCreate(savedInstanceState)
    }

    override fun onDestroy() {
        super.onDestroy()
        if(!actSub.isDisposed) actSub.dispose()
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return attachAdapters(setupViewBinding(inflater, container))
    }

    private fun setupViewBinding(inflater: LayoutInflater?, container: ViewGroup?): View {
        val b = FragmentRoundsBinding.inflate(inflater, container, false)
        b.tournament = td.tournament
        return b.root
    }

    private fun attachAdapters(v: View): View {
        val adapter = RoundListExpandableAdapter(td, v.context, this, bus)
        v.expandRoundList.setAdapter(adapter)
        v.expandRoundList.expandGroup(0)
        return v
    }

    fun update(td: TournamentData){
        this.td = td
        view?.let{
            attachAdapters(it)
        }
    }
}
