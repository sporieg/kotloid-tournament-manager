package org.netrunner

import android.app.Application
import android.content.Context
import dagger.Component
import dagger.Module
import dagger.Provides
import io.paperdb.Paper
import org.netrunner.data.*
import org.netrunner.data.Paper.FactionDataManager
import org.netrunner.data.Paper.Library
import org.netrunner.data.Paper.PaperTournamentManager
import org.netrunner.kotloid.MainNavComponent
import org.netrunner.kotloid.MainNavModule
import org.netrunner.kotloid.RunTournamentComponent
import org.netrunner.kotloid.RunTournamentModule
import org.netrunner.kotloid.factions.EditFactionComponent
import org.netrunner.kotloid.factions.EditIdentityComponent
import org.netrunner.kotloid.players.EditPlayerComponent
import org.netrunner.kotloid.ranking.PlayerRankDetailsComponent
import org.netrunner.kotloid.rounds.ReportRoundResultsActivityComponent
import org.netrunner.kotloid.tournaments.TournamentHeaderComponent
import org.netrunner.nrdb.Importer
import org.netrunner.nrdb.NrdbApi
import org.netrunner.rx.RxBus
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Inject

class App: Application()  {
    @Inject
    lateinit var dataManager: ITournamentDataManager
    @Inject
    lateinit var factionManager: IFactionDataManager
    @Inject
    lateinit var tournamentEvents: TournamentEventRouter
    @Inject
    lateinit var factionEvents: FactionEventRouter
    @Inject
    lateinit var importer: Importer


    override fun onCreate() {
        super.onCreate()
        Paper.init(baseContext)
        appComponent = DaggerAppComponent.builder()
                .appModule(AppModule(this))
                .build()
        appComponent.inject(this)
        factionManagerOld = factionManager
        Library.tournamentsBook().destroy()
        Library.tournamentHeaderBook().destroy()
        try{
            sampleData(dataManager, factionManager)
        }catch(e: Exception){}
    }

    companion object {
        lateinit var appComponent: AppComponent
        var factionManagerOld: IFactionDataManager = InMemoryFactionDataManager()
    }
}

@Module
class AppModule(val app: App){
    @Provides
    @ApplicationScope
    fun provideContext(): Context {
        return app
    }

    @Provides
    @ApplicationScope
    fun provideApp(): App{
        return app
    }

    @Provides
    @ApplicationScope
    fun library(): Library = Library

    @Provides
    @ApplicationScope
    fun tournamentDataManager(l: Library): ITournamentDataManager {
        return PaperTournamentManager(l.tournamentHeaderBook(), l.tournamentsBook())
    }

    @Provides
    @ApplicationScope
    fun factionManager(l: Library): IFactionDataManager = FactionDataManager(l.factionBook(), l.identityBook())

    @Provides
    @ApplicationScope
    fun bus(): RxBus = RxBus()

    @Provides
    @ApplicationScope
    fun tournamentEventSubscriber(factions: IFactionDataManager, tournaments: ITournamentDataManager, bus: RxBus): TournamentEventRouter =
            TournamentEventRouter( tournaments,factions, bus)

    @Provides
    @ApplicationScope
    fun factionEventRouter(factions: IFactionDataManager, bus: RxBus): FactionEventRouter = FactionEventRouter(factions, bus)


    @Provides
    @ApplicationScope
    fun retrofit(): Retrofit = Retrofit.Builder()
            .baseUrl(NrdbApi.NRDB_API_URL)
            .addConverterFactory(MoshiConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()

    @Provides
    @ApplicationScope
    fun importer(retrofit:Retrofit, bus: RxBus): Importer = Importer(retrofit.create(NrdbApi::class.java), bus)
}

@ApplicationScope
@Component(modules = arrayOf(AppModule::class))
interface AppComponent{
    fun inject(app: App)
    fun newMainNavComponent(module: MainNavModule): MainNavComponent
    fun newRunTournamentComponent(module: RunTournamentModule): RunTournamentComponent
    fun newEditPlayerComponent(): EditPlayerComponent
    fun newEditFactionComponent(): EditFactionComponent
    fun newEditIdentityComponent(): EditIdentityComponent
    fun newTournamentHeaderComponent(): TournamentHeaderComponent
    fun newPlayerRankDetailsComponent(): PlayerRankDetailsComponent
    fun newPlayerRoundResultsComponent(): ReportRoundResultsActivityComponent
}

