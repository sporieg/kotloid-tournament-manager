package org.netrunner.tournament

typealias PlayerId = Int

/*
  A Player in a tournament.

  Players only exist relative to a tournament, to add players to a tournament use an appropriate function on tournament.
 */
class Player (val id: PlayerId,
              var nickName: String,
              var corp: Identity,
              var runner: Identity,
              var dropped: Boolean = false){
    companion object {
        fun nextId(l: List<Player>): PlayerId = l.count() + 1
    }

    override fun toString(): String {
        return "$id $nickName $corp $runner"
    }

    /**
     * Only the id determines uniqueness/equality
     */
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other?.javaClass != javaClass) return false

        other as Player

        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        return id
    }
}

class Players(val players: MutableList<Player>){
    fun activePlayers(): List<Player> = players.filter { !it.dropped }

    fun addPlayer(nickName: String, corp: Identity, runner: Identity): Player {
        val p =  Player(Player.nextId(players), nickName, corp, runner)
        players.add(p)
        return p
    }
    fun removePlayer(p: Player){
        players.remove(p)
    }
    fun updatePlayer(id: PlayerId, playerEdits: (Player) -> Unit = {}){
        player(id)?.let {
            playerEdits(it)
        }
    }
    fun player(id: PlayerId) = players.firstOrNull { it.id == id }
    fun activePlayer(id: PlayerId) = activePlayers().firstOrNull { it.id == id }
}

