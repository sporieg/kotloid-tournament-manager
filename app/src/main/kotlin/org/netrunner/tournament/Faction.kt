package org.netrunner.tournament

typealias FactionId = String

data class Faction(val id: FactionId = "invalid", val color: String = "ff3300", val isMini: Boolean = false, val name: String = "Missing", val sideCode: String = "invalid"){
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other?.javaClass != javaClass) return false
        other as Faction
        if (id != other.id) return false
        return true
    }
    override fun hashCode(): Int {
        return id.hashCode()
    }

}

