package org.netrunner.tournament

typealias IdentityId = String

data class Identity(val code: IdentityId = "invalid", val name: String = "Invalid", val factionId: FactionId = "Missing"){
    companion object{
        val RUNNER = "runner"
        val CORP = "corp"
    }
    override fun toString(): String = name
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other?.javaClass != javaClass) return false
        other as Identity
        if (code != other.code) return false
        return true
    }
    override fun hashCode(): Int {
        return code.hashCode()
    }
}