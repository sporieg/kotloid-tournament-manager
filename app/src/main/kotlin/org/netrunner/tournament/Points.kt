package org.netrunner.tournament

enum class Points(val points: Int){
    Loss(0),
    Draw(1),
    ModifiedWin(2),
    Win(3);
    fun matches(p: Int): Boolean = p == points
    companion object {
        fun all(): List<Points> {
            return listOf(Loss, Draw, ModifiedWin, Win)
        }
    }
}