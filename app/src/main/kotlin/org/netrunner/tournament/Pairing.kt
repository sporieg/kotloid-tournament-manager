package org.netrunner.tournament

interface Pairing {
    fun involves(p: PlayerId): Boolean
    var round: Int
}

