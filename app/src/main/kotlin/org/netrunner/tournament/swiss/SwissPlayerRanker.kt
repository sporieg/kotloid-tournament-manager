package org.netrunner.tournament.swiss

val random = java.security.SecureRandom()

class SwissRanker(val t: SwissTournament){
    fun pointsFor(p: org.netrunner.tournament.PlayerId) = t.swissSwissRounds.sumBy { it.pointsFor(p) }
    fun rankPlayers(p: List<org.netrunner.tournament.Player>) =
        p.sortedWith(compareBy<org.netrunner.tournament.Player>(
        { pointsFor(it.id) },
        { it.strengthOfSchedule(t) },
        { it.extendedStrengthOfSchedule(t)},
        { org.netrunner.tournament.swiss.random.nextInt(t.activePlayers().count())}))
        .reversed()
    fun rankedPlayers(): List<org.netrunner.tournament.Player> =  rankPlayers(t.players)
}