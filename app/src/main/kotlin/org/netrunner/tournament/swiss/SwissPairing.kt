package org.netrunner.tournament.swiss

import org.netrunner.tournament.PlayerId

interface SwissPairing{
    fun pointsFor(p: PlayerId): Int
}