package org.netrunner.tournament.swiss

import org.netrunner.tournament.Pairing
import org.netrunner.tournament.PlayerId

class SwissPlayerPairing(val p1: PlayerId, val p2: PlayerId, override var round: Int = 0, val match: SwissMatch = SwissMatch(SwissGame(p1, p2), SwissGame(p2, p1))): Pairing, SwissPairing {

    override fun pointsFor(p: PlayerId) = match.pointsFor(p)
    override fun involves(p: PlayerId) = p1 == p || p2 == p
    fun player1Points() = match.pointsFor(p1)
    fun player2Points() = match.pointsFor(p2)

    fun opponentOf(p: PlayerId): PlayerId {
        if(!involves(p)) throw Exception("Player did not play in this pairing")
        if(p1 == p) return p2
        return p1
    }
    override fun toString(): String {
        return "SwissPlayerPairing(p1=$p1, p2=$p2)"
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other?.javaClass != javaClass) return false

        other as SwissPlayerPairing

        if (p1 != other.p1 && p1 != other.p2) return false
        if (p2 != other.p2 && p2 != other.p1) return false
        return true
    }

    override fun hashCode(): Int {
        var result = p1.hashCode()
        result = 31 * result + p2.hashCode()
        result = 31 * result + match.hashCode()
        return result
    }
}