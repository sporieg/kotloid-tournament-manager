package org.netrunner.tournament.swiss

import org.netrunner.tournament.Player


fun List<SwissRound>.pointsFor(p: org.netrunner.tournament.PlayerId) = this.sumBy { it.pointsFor(p) }

/*
  Tracks stats about an individual person in the tournament, including points
  Strength of Schedule, and Extended Strength of Schedule.
 */
fun Player.points(t: SwissTournament): Int = t.swissSwissRounds.pointsFor(id)
fun Player.rounds(t: SwissTournament): Int = t.roundHistoryFor(this).size
fun Player.avgPoints(t: SwissTournament): Double = t.swissSwissRounds.pointsFor(this.id).toDouble() / rounds(t).toDouble()
fun Player.opponents(t: SwissTournament): List<org.netrunner.tournament.Player> = t.swissSwissRounds.flatMap {
    it.pairings.filterIsInstance<SwissPlayerPairing>()
            .filter { it.involves(this.id) } }
        .map { t.activePlayer(it.opponentOf(this.id))!! }

/*
 A editPlayer’corpIdKey strength of schedule
    is calculated by dividing each opponent’corpIdKey total tournament
    points by the number of rounds that opponent has played,
    adding the results of each opponent played, and then dividing
    that total by the number of opponents the editPlayer has played.
    The editPlayer with the highest strength of schedule is ranked
    above all other players in the group not yet ranked. The editPlayer
    with the second-highest strength of schedule is ranked second
    among all players in the group not yet ranked, and so on
 */
fun org.netrunner.tournament.Player.strengthOfSchedule(t: SwissTournament): Double = pointsCalc(t){
    it.avgPoints(t)
}

fun org.netrunner.tournament.Player.extendedStrengthOfSchedule(t: SwissTournament): Double =  pointsCalc(t){
    it.strengthOfSchedule(t)
}

fun org.netrunner.tournament.Player.pointsCalc(t: SwissTournament, func: (org.netrunner.tournament.Player) -> Double): Double {
    val his = t.matchHistoryFor(this)
    if(his.isEmpty()){
        return 0.0
    }
    return his.map { m -> func(t.activePlayer(m.opponentOf(this.id))!!) }.average()
}