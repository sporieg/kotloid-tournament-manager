package org.netrunner.tournament.swiss

import org.netrunner.tournament.Identity
import org.netrunner.tournament.Pairing
import org.netrunner.tournament.Player
import org.netrunner.tournament.PlayerId


class SwissTournament(val players: MutableList<Player> = mutableListOf(), var swissSwissRounds: List<SwissRound> = listOf()) {
    fun round(roundNum: Int) = swissSwissRounds[roundNum -1]
    fun activePlayers(): List<Player> = players.filter { !it.dropped }
    fun byePlayers(): List<Player> = activePlayers().filter { p ->
            val byes = swissSwissRounds.flatMap{it.byes()}
            byes.any { b -> b.involves(p.id) }
        }
    fun noByePlayers(): List<Player> = activePlayers().filterNot { p ->
            val byes = swissSwissRounds.flatMap{it.byes()}
            byes.any { b -> b.involves(p.id) }
        }
    fun addPlayer(nickName: String, corp: Identity, runner: Identity): Player {
        val p =  Player(Player.nextId(players), nickName, corp, runner)
        players.add(p)
        return p
    }
    fun removePlayer(p: Player){
        if(matchHistoryFor(p).any()) throw Error("Cannot remove a player who has played, try dropping instead.")
        players.remove(p)
    }
    fun updatePlayer(id: PlayerId, playerEdits: (Player) -> Unit = {}){
        player(id)?.let {
            playerEdits(it)
        }
    }
    fun player(id: PlayerId) = players.firstOrNull { it.id == id }
    fun activePlayer(id: PlayerId) = activePlayers().firstOrNull { it.id == id }
    /**
     * Returns just a list of the pairings the player has played.
     */
    fun matchHistoryFor(p: Player): List<SwissPlayerPairing> = swissSwissRounds.flatMap {
        it.pairings.filterIsInstance<SwissPlayerPairing>().filter { pair -> pair.involves(p.id) }
    }
    //Returns a list of pairings for the player in swissRound order
    fun roundHistoryFor(p: PlayerId): List<Pairing> = swissSwissRounds.flatMap {
        it.pairings //To keep the pairings in order.
           .filter{ pair -> pair.involves(p) }
    }
    fun roundHistoryFor(p: Player): List<Pairing> = roundHistoryFor(p.id)
    fun addRound(nextSwissRound: SwissRound = SwissRound(swissSwissRounds.count() + 1, listOf())) {
        swissSwissRounds += nextSwissRound
    }
    fun dropLast(n: Int = 1) {
        swissSwissRounds = swissSwissRounds.dropLast(n)
    }
    fun assignPairings(r: SwissRound){
        val updated = swissSwissRounds.toMutableList()
        updated[r.num - 1] = r
        swissSwissRounds = updated
    }
}

