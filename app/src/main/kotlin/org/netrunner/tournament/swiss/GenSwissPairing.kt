package org.netrunner.tournament.swiss

import org.netrunner.tournament.Pairing

/*
  Creates a swissRound pairing for the current tournament, assumes you want to pair the last swissRound,
  and that the current swissRound is unreported, eg: 0 points, no byes, etc.
  If you try to re-pair a swissRound with a bye using this class, the bye points will be taken into account.
  Use SwissRoundGenerator instead.
 */
class GenSwissPairing(val t: SwissTournament, val roundNum: Int){
    val activePlayers = t.activePlayers()
    val noByePlayers = t.noByePlayers()
    val ranker = SwissRanker(t)
    fun List<org.netrunner.tournament.Player>.rank() = ranker.rankPlayers(this)
    fun org.netrunner.tournament.Player.opponents() = this.opponents(t)
    fun Int.isOdd() = this % 2 == 1

    fun genPairing(): List<Pairing> {
        if (activePlayers.count().isOdd()) {
            val byePlayer = awardBye()
            return listOf(SwissBye(byePlayer, roundNum)).plus(assignPlayer(activePlayers.minus(byePlayer)))
        }
        return assignPlayer(activePlayers)
    }

    fun awardBye(): org.netrunner.tournament.Player =  noByePlayers.rank().last()

    /*
      Pairs out the available players.  Assumes availablePlayers has an even count.
     */
    private fun assignPlayer(availablePlayers: List<org.netrunner.tournament.Player>):  List<Pairing> {
        if (availablePlayers.isEmpty() || availablePlayers.count().isOdd()) return listOf()
        fun pairNextPlayer(players: List<org.netrunner.tournament.Player>): List<Pairing>{
            val p1 = players.rank().first()
            val p1HasPlayed = p1.opponents()
            fun pairWithP1(remainingPlayers: List<org.netrunner.tournament.Player>, triedToPairWith: List<org.netrunner.tournament.Player>): List<Pairing>{
                val playerToTry = remainingPlayers.filter { !p1HasPlayed.contains(it) && ! triedToPairWith.contains(it)}
                //If could not get another player, return empty pairing.
                val p2 = playerToTry.rank().firstOrNull() ?: return listOf()
                val pair = SwissPlayerPairing(p1.id, p2.id, roundNum)
                val leftovers = remainingPlayers.minus(p2)
                //We have no one else to try
                if(leftovers.isEmpty()) return listOf(pair)
                val otherPairings = pairNextPlayer(leftovers)
                //we could not pair the rest of the tournament
                if(otherPairings.isEmpty()) return pairWithP1(remainingPlayers, triedToPairWith.plus(p2))
                //All good
                return listOf(pair).plus(otherPairings)
            }
            return pairWithP1(players.minus(p1), listOf())
        }
        return pairNextPlayer(availablePlayers)
    }
}