package org.netrunner.tournament.swiss

/*
  A org.netrunner.swiss match consists of two games
 */
class SwissMatch(val game1: SwissGame, val game2: SwissGame){
    fun pointsFor(p: org.netrunner.tournament.PlayerId) = game1.pointsFor(p) + game2.pointsFor(p)
    fun runnerGameFor(p: org.netrunner.tournament.PlayerId): SwissGame {
        if(game1.runnerPlayer == p) return game1
        if(game2.runnerPlayer == p) return game2
        throw Error("Player not involved in match")
    }
    fun corpGameFor(p: org.netrunner.tournament.PlayerId): SwissGame {
        if(game1.corpPlayer == p) return game1
        if(game2.corpPlayer == p) return game2
        throw Error("Player not involved in match")
    }
}