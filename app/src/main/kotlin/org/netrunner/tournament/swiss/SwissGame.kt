package org.netrunner.tournament.swiss

import org.netrunner.tournament.PlayerId

enum class WinType{
    Points,
    FlatlineMeat,
    FlatlineNet,
    IntentionalDraw
}

data class SwissGame(val corpPlayer: PlayerId,
                     val runnerPlayer: PlayerId,
                     var corpPoints: Int = 0,
                     var runnerPoints: Int = 0,
                     var winType: WinType = WinType.Points){
    fun pointsFor(p: PlayerId): Int {
        if(corpPlayer == p) return corpPoints
        if(runnerPlayer == p) return runnerPoints
        return 0
    }
    fun setPointsFor(p: PlayerId, points: Int){
        if(corpPlayer == p ) corpPoints = points
        if(runnerPlayer == p) runnerPoints = points
    }
}