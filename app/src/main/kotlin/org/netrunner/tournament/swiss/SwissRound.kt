package org.netrunner.tournament.swiss

import org.netrunner.tournament.Pairing
import org.netrunner.tournament.PlayerId


class SwissRound(val num: Int, val pairings: List<Pairing>) {
    fun pointsFor(p: PlayerId) = pairings.filter { it.involves(p) }.sumBy {
        when(it){
            is SwissPairing -> {
                it.pointsFor(p)
            }
            else -> 0
        }
    }
    fun between(p1: PlayerId, p2: PlayerId) = pairings.first { it.involves(p1) && it.involves(p2) } as SwissPlayerPairing
    fun byes(): List<SwissBye> = pairings.filterIsInstance<SwissBye>()
    override fun toString(): String {
        return "SwissRound(num=$num, pairings=$pairings)"
    }
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other?.javaClass != javaClass) return false

        other as org.netrunner.tournament.swiss.SwissRound

        if (num != other.num) return false
        if (pairings != other.pairings) return false

        return true
    }
    override fun hashCode(): Int {
        var result = num
        result = 31 * result + pairings.hashCode()
        return result
    }
}