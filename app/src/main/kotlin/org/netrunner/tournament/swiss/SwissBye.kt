package org.netrunner.tournament.swiss

import org.netrunner.tournament.Pairing
import org.netrunner.tournament.Player
import org.netrunner.tournament.PlayerId

/*
 No games are played with a bye, the editPlayer with a bye just gets 2 wins against a theoretical worst editPlayer.
 */
class SwissBye(val byePlayer: PlayerId, override var round: Int = 0): Pairing, SwissPairing {
    constructor(p: Player, round: Int = 0): this(p.id, round)
    override fun pointsFor(p: PlayerId): Int {
        if(p == byePlayer) return 6
        return 0
    }
    override fun involves(p: PlayerId) = byePlayer == p
    override fun toString(): String {
        return "SwissBye(byePlayer=$byePlayer)"
    }
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other?.javaClass != javaClass) return false

        other as SwissBye

        if (byePlayer != other.byePlayer) return false

        return true
    }
    override fun hashCode(): Int {
        return byePlayer.hashCode()
    }
}