package org.netrunner.tournament.swiss

class SwissRoundGenerator(private val startingTournament: SwissTournament){
    lateinit var t: SwissTournament

    /*
      Always pairs the provided swissRound as the last swissRound.  eg: If you pair swissRound 2 of 4 again, pairs 3,4 are ignored.
     */
    fun pairRound(roundNum: Int): SwissRound {
        val idx = roundNum - 1
        if (idx >= startingTournament.swissSwissRounds.count()) throw Error("Invalid swissRound pairing")
        //Create a new tournament with just the rounds up to what we are pairing.
        //This prevents pairing based on data past the current swissRound or more likely
        // Prevents repairing an already assigned swissRound from getting polluted by bys/points already reported.
        t = SwissTournament(startingTournament.players,
                //Ignore pairings at or above current swissRound.
                startingTournament.swissSwissRounds.take(idx))
        val pairs = GenSwissPairing(t, roundNum).genPairing()
        return SwissRound(roundNum, pairs)
    }

}