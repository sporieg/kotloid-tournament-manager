package org.netrunner.routing

import android.content.Context
import android.content.Intent
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.util.Log
import org.netrunner.kotloid.R
import org.netrunner.kotloid.RunTournamentActivity
import java.util.*

sealed class Navigation {
    class Routable(val fragment: Fragment, val title: Int): Navigation()
    class ActivityIntent(val clazz: Class<RunTournamentActivity>, val id: UUID, val intentBuilder: (ActivityIntent, Context) -> Intent) : Navigation(){
        fun intent( packageContext: Context): Intent{
            return intentBuilder(this, packageContext)
        }
    }
    object UnRoutable: Navigation()
}