package org.netrunner.routing

import java.util.*


typealias TopLevelRoute = () -> Navigation

class SimpleRouter {
    companion object {
        private val default: TopLevelRoute= fun() = Navigation.UnRoutable
    }
    val routes = mutableMapOf<Int, TopLevelRoute>()
    operator fun get(id: Int): Navigation {
        val func = routes.getOrElse(id, { default })
        return func()
    }
    operator fun set(id: Int, cons: TopLevelRoute){
        routes[id] = cons
    }
}